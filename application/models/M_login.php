<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model
{
	public function signin($where)
	{
		$query = $this->db->get_where('login', $where);
		return $query;
	}
}
