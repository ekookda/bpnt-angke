<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model
{
	public function show_table($table, $title=NULL, $sort='asc')
	{
		$this->db->order_by($title, $sort);
		$query = $this->db->get($table);

		return $query;
	}

	public function insert_table($table, $data)
	{
		$insert = $this->db->insert($table, $data);

		return $insert;
	}

	public function get_where($table, $where, $title=NULL, $sort='asc')
	{
		$this->db->order_by($title, $sort);
		$where = $this->db->get_where($table, $where);

		return $where;
	}

	public function get_max($column, $table)
	{
		$this->db->select_max($column);
		$query = $this->db->get($table);

		return $query;
	}

	public function update($table, $data, $id)
	{
		$update = $this->db->update($table, $data, $id);

		return $update;
	}

	public function delete($table, $data)
	{
		$delete = $this->db->delete($table, $data);

		return $delete;
	}

	public function count_rt()
	{
		$query = "	SELECT
					COUNT(IF(rt='001', rt, null)) AS rt_0 ,
					COUNT(IF(rt='002', rt, null)) AS rt_1 ,
					COUNT(IF(rt='003', rt, null)) AS rt_2 ,
					COUNT(IF(rt='004', rt, null)) AS rt_3 ,
					COUNT(IF(rt='005', rt, null)) AS rt_4 ,
					COUNT(IF(rt='006', rt, null)) AS rt_5 ,
					COUNT(IF(rt='007', rt, null)) AS rt_6 ,
					COUNT(IF(rt='008', rt, null)) AS rt_7 ,
					COUNT(IF(rt='009', rt, null)) AS rt_8 ,
					COUNT(IF(rt='010', rt, null)) AS rt_9 ,
					COUNT(IF(status='1', status, null)) AS jumlah
					FROM warga
					WHERE status='1'
				";
		// $query = "SELECT *, COUNT(IF(rt='001', rt, null)) AS rt FROM warga WHERE rt='001'";
		$get = $this->db->query($query);

		return $get;
	}

}
