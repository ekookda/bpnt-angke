<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>::. Data Unit Usaha | BPNT .::</title>

    <!-- Bootstrap -->
    <link href="<?=base_url('assets/node_modules/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">

	<!-- CSS DataTables Bootstrap -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.jqueryui.min.css">
</head>
<body>
<div id="">
	<div id="heading" style=""></div>
	<div class="table-responsive">
		<table id="printTable" class="display table table-hover" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Nama Agen</th>
					<th class="text-center">Jenis Usaha</th>
					<th class="text-center">Alamat</th>
				</tr>
			</thead>
			<tbody>
			<?php
				if (!$data_tbagen || $data_tbagen == NULL):
					// Tidak ada data

				else:
					$no = 1;
					foreach ($data_tbagen->result() as $r):
			?>
						<tr>
							<td class="text-center"><?=$no++;?></td>
							<td class="text-center"><?=$r->nama_agen;?></td>
							<td class="text-center"><?=$r->jenis_usaha;?></td>
							<td class="text-center"><?=$r->alamat;?></td>
						</tr>
			<?php
					endforeach;
				endif;
			?>
			</tbody>
		</table>
	</div>
</div>

<?php $this->load->view('template/javascript'); ?>

<!-- Datatables JS -->
<script src="<?=base_url('assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.jqueryui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#printTable').DataTable();
	});
</script>

</body>
</html>
