<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>::. Data Warga | BPNT .::</title>

    <!-- Bootstrap -->
    <link href="<?=base_url('assets/node_modules/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">

	<!-- CSS DataTables Bootstrap -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.jqueryui.min.css">
</head>
<body>
<div id="">
	<div id="heading" style=""></div>
	<div class="table-responsive">
		<table id="printTable" class="display table table-hover" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">RT / RW</th>
					<th class="text-center">Tanggal Transfer</th>
				</tr>
			</thead>
			<tbody>
			<?php
				if (!$data_tbtransfer || $data_tbtransfer == NULL):
					// Tidak ada data

				else:
					$no = 1;
					foreach ($data_tbtransfer->result() as $r):
			?>
						<tr>
							<td class="text-center"><?=$no++;?></td>
							<td class="text-center"><?=$r->nomor_rt . " / " . $r->nomor_rw;?></td>
							<?php
								$bulan = [
									1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni',
									7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'
								];
							?>
							<td class="text-center"><?=date("d", strtotime($r->tanggal_transfer)). " " .$bulan[date("m", strtotime($r->tanggal_transfer))]. " " .date("Y", strtotime($r->tanggal_transfer));?></td>
						</tr>
			<?php
					endforeach;
				endif;
			?>
			</tbody>
		</table>
	</div>
</div>

<?php $this->load->view('template/javascript'); ?>

<!-- Datatables JS -->
<script src="<?=base_url('assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.jqueryui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#printTable').DataTable();
	});
</script>

</body>
</html>
