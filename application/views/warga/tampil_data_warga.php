<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>::. Data Warga | BPNT .::</title>

    <!-- Bootstrap -->
    <link href="<?=base_url('assets/node_modules/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">

	<!-- CSS DataTables Bootstrap -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.jqueryui.min.css">
</head>
<body>
<div id="">
	<div id="heading" style=""></div>
	<div class="table-responsive">
		<table id="printTable" class="display table table-hover" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">No KTP</th>
					<th class="text-center">Nama Lengkap</th>
					<th class="text-center">RT/RW</th>
					<th class="text-center">Status</th>
				</tr>
			</thead>
			<tbody>
			<?php
				if (!$data_tbwarga || $data_tbwarga == NULL):
					// Tidak ada data

				else:
					$no = 1;
					foreach ($data_tbwarga->result() as $r):
			?>
						<tr>
							<!-- <td><?=$r->id_ktp;?></td> -->
							<td class="text-center"><?=$no++;?></td>
							<td class="text-center"><?=ucwords($r->id_ktp);?></td>
							<td><?=ucwords($r->nama_lengkap);?></td>
							<td class="text-center"><?=$r->rt . "/" . $r->rw;?></td>
							<?php
							switch ($r->status):
								case 1:
									$status = "<span class='label label-success'>Berhak</span>";
									break;
								default:
									$status = "<span class='label label-danger'>Belum</span>";
									break;
							endswitch;
							?>
							<td class="text-center"><?=ucfirst($status);?></td>
						</tr>
			<?php
					endforeach;
				endif;
			?>
			</tbody>
		</table>
	</div>
</div>

<?php $this->load->view('template/javascript'); ?>

<!-- Datatables JS -->
<script src="<?=base_url('assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.jqueryui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#printTable').DataTable();
	});
</script>

</body>
</html>
