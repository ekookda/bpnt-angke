<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> LOGIN | SIMBPNT </title>

    <!-- Bootstrap -->
    <link href="<?=base_url('assets/node_modules/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=base_url('assets/node_modules/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?=base_url('assets/gentelella/vendors/nprogress/nprogress.css');?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?=base_url('assets/gentelella/vendors/animate.css/animate.min.css');?>" rel="stylesheet">
    <!-- Custom Theme Style -->
	<link href="<?=base_url('assets/gentelella/build/css/custom.min.css');?>" rel="stylesheet">

	<?php $this->load->view('template/javascript.php'); ?>
	<!-- Parsley JS -->
	<script src="<?=base_url('assets/node_modules/parsleyjs/dist/parsley.js');?>"></script>
	<script src="<?=base_url('assets/node_modules/parsleyjs/dist/i18n/id.js');?>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#form_login').parsley();
		});
	</script>
	<style>
		.login_wrapper {
			right: 0;
			margin: 2% auto 0;
			max-width: 350px;
			position: relative;
		}
	</style>
</head>

<body class="login">
	<div>
		<div class="container-fluid">
			<br>
			<div class="row">
				<div class="col-sm-offset-4 col-sm-4">
					<img src="<?=base_url('assets/gentelella/production/images/banner_angke.jpg');?>" class="img-responsive img-thumbnail" style="width:100%">
				</div>
			</div>
		</div>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
					<?php if (validation_errors()): ?>
						<div class="alert alert-danger">
							<?php echo validation_errors(); ?>
						</div>
					<?php endif; ?>

					<!-- Form Open -->
					<?= form_open(site_url('welcome/validation'), array('class'=>'form', 'role'=>'form', 'id'=>'form_login', 'data-parsley-validate'=>'')); ?>
						<h1>Login Form</h1>
						<div>
							<?php
							$user_attribute = array(
								'name' => 'username',
								'id' => 'username',
								'class' => 'form-control',
								'placeholder' => 'Username',
								'value' => set_value('username'),
								'data-parsley-required'=>'true'
							);
							echo form_input($user_attribute);
							?>
						</div>
						<div>
							<?php
							$pass_attribute = array(
								'name' => 'password',
								'id' => 'password',
								'class' => 'form-control',
								'placeholder' => 'Password',
								'data-parsley-required'=>'true'
							);
							echo form_password($pass_attribute);
							?>
						</div>

						<div class="clearfix"></div>

						<br>

						<div>
							<?php
							$submit = array(
								'name' => 'btn-submit',
								'id' => 'btn-submit',
								'class' => 'btn btn-primary btn-block submit',
								'value' => 'true',
								'type' => 'submit',
								'content' => '<i class="fa fa-sign-in"></i> Sign In'
							);
							echo form_button($submit);
							?>
						</div>
					<?php echo form_close(); ?>
					<div class="separator">
						<!-- <p class="change_link">New to site?
							<a href="#signup" class="to_register"> Create Account </a>
						</p> -->

						<div>
							<h1><i class="fa fa-paw"></i> SIMBPNT </h1>
							<p>©2016 All Rights Reserved. <br>Sistem Informasi Manajemen BPNT - Angke</p>
						</div>
					</div> <!-- /.separator -->
        		</section>
        	</div> <!-- /.login_form -->
    	</div> <!-- /.login_wrapper -->
    </div>

<?php if ($this->session->flashdata('error')): ?>
	<script type="text/javascript">
		swal('Oops!', "<?=$this->session->flashdata('error');?>", 'error');
	</script>
<?php elseif($this->session->flashdata('success')): ?>
	<script type="text/javascript">
		swal('Success!', "<?=$this->session->flashdata('success');?>", 'success');
	</script>
<?php endif; ?>
</body>
</html>
