<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/main'); ?>
<?php $this->load->view('template/sidebar-menu'); ?>
<?php $this->load->view('template/top-navigation'); ?>

            <!-- Page Content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left"></div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
									<?= form_open('admin/storejadwal', array('class'=>'form-horizontal form-label-left', 'id'=>'formData')); ?>
                                        <span class="section"><i class="fa fa-info-circle"></i> Input Jadwal Transfer</span>

										<?php
											if (validation_errors()) {
												echo "<div class='alert alert-danger'>".validation_errors()."</div>";
											}
										?>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_agen">Nomor RT <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
												<select name="nomor_rt" id="nomor_rt" class="form-control" required="">
												<?php
													$nomorRt = ['001', '002', '003', '004', '005', '006', '007', '008', '009', '010'];
													foreach ($nomorRt as $key):
												?>
														<option value="<?=$key?>"><?=$key?></option>
												<?php
													endforeach;
												?>
												</select>
                                            </div>
										</div>

										<!-- NOMOR RW -->
										<div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor_rw">Nomor RW <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
												<select name="nomor_rw" id="nomor_rw" class="form-control" required="">
												<?php
												// Memunculkan nomor RW yang sudah diinput
												$where = array('rt'=>'0');
												$get_where = $this->m_admin->get_where('rtrw', $where);
												if ($get_where->num_rows() > 0):
													foreach ($get_where->result() as $key):
												?>
														<option value="<?=$key->rw?>"><?=$key->rw?></option>
												<?php
													endforeach;
												endif;
												?>
												</select>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_usaha">Tanggal Transfer <span class="required text-danger">*</span></label>
                                        	<div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="date" id="tanggal_transfer" name="tanggal_transfer" value="<?=set_value('tanggal_transfer');?>" required="required" class="form-control col-md-7 col-xs-12" placeholder="Contoh: Agen Beras">
                                            </div>
                                        </div>

                                        <div class="ln_solid"></div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-3">
                                                <button id="send" name="btn-send" type="submit" class="btn btn-success" value="btn-value">Submit</button>
                                            </div>
                                        </div>
									<?=form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

<?php $this->load->view('template/footer'); ?>
<?php $this->load->view('template/javascript'); ?>

<script src="<?=base_url('assets/gentelella/vendors/validator/validator.js');?>"></script>
<?php if ($this->session->flashdata('error')): ?>
<script type="text/javascript">
	swal('Oops!', "<?=$this->session->flashdata('error');?>", 'error');
</script>
<?php
endif;
$this->load->view('template/end');
?>
