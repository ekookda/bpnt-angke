<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/main'); ?>
<?php $this->load->view('template/sidebar-menu'); ?>
<?php $this->load->view('template/top-navigation'); ?>

            <!-- Page Content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left"></div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
								<?php
									if (validation_errors()) {
										echo "<div class='alert alert-danger'>".validation_errors()."</div>";
									}

									if ($data == NULL):
										# code...
									else:
										foreach ($data as $r):
											$hidden = array('id' => $r->id_jadwal);
										echo form_open('admin/updatejadwal/'.$r->id_jadwal, array('class'=>'form-horizontal form-label-left', 'id'=>'formData'), $hidden);
								?>
                                        	<span class="section"><i class="fa fa-info-circle"></i> Edit Jadwal Transfer</span>

											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor_rt">Nomor RT <span class="required text-danger">*</span></label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<select name="nomor_rt" id="nomor_rt" class="form-control" required="">
													<?php
														$nomorRt = ['001', '002', '003', '004', '005', '006', '007', '008', '009', '010'];
														$select = $r->nomor_rt;
														foreach ($nomorRt as $key):
													?>
															<option value="<?=$key;?>" <?php if ($select == $key) echo "selected='selected'";?>><?=$key?></option>
													<?php
														endforeach;
													?>
													</select>
												</div>
											</div>

											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor_rw">Nomor RW <span class="required text-danger">*</span></label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<select name="nomor_rw" id="nomor_rw" class="form-control" required="">
														<?php
														$id_jadwal = ['id_jadwal' => $r->id_jadwal];
														$transfer  = $this->m_admin->get_where('transfer', $id_jadwal);
														// Memunculkan nomor RW yang sudah diinput
														$where = ['rt' => '0'];
														$rtrw  = $this->m_admin->get_where('rtrw', $where);

														if ($rtrw->num_rows() > 0 && $transfer->num_rows() == 1):
															// ambil data dari transfer sesuai id_jadwal
															$result_transfer   = $transfer->result();
															$transfer_nomor_rw = $result_transfer[0]->nomor_rw;

															// ambil data dari rtrw untuk nomor rw
															$selected = 'selected="selected"';
															foreach ($rtrw->result() as $key):
														?>
																<option value="<?= $key->rw; ?>" <?php if ($transfer_nomor_rw == $key->rw) echo $selected; ?>><?= $key->rw; ?></option>
														<?php
															endforeach;

														endif;
														?>
													</select>
												</div>
											</div>

											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_lama">Tanggal Sebelumnya</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<input type="text" disabled id="tanggal_lama" name="tanggal_lama" value="<?=date("d-m-Y", strtotime($r->tanggal_transfer));?>" required="required" class="form-control col-md-7 col-xs-12">
												</div>
											</div>

											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_transfer">Tanggal Transfer <span class="required text-danger">*</span></label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<input type="date" id="tanggal_transfer" name="tanggal_transfer" value="<?=ucwords($r->tanggal_transfer);?>" required="required" class="form-control col-md-7 col-xs-12">
												</div>
											</div>

											<div class="ln_solid"></div>

											<div class="form-group">
												<div class="col-md-6 col-md-offset-3">
													<button id="send" name="btn-send" type="submit" class="btn btn-success" value="btn-value">Submit</button>
												</div>
											</div>
								<?php
										echo form_close();
									endforeach;
									endif;
								?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

<?php $this->load->view('template/footer'); ?>
<?php $this->load->view('template/javascript'); ?>

<script src="<?=base_url('assets/gentelella/vendors/validator/validator.js');?>"></script>
<?php if ($this->session->flashdata('error')): ?>
<script type="text/javascript">
	swal('Oops!', "<?=$this->session->flashdata('error');?>", 'error');
</script>
<?php
endif;
$this->load->view('template/end');
?>
