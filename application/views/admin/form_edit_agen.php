<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/main'); ?>
<?php $this->load->view('template/sidebar-menu'); ?>
<?php $this->load->view('template/top-navigation'); ?>

            <!-- Page Content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left"></div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
								<?php
									if (validation_errors()) {
										echo "<div class='alert alert-danger'>".validation_errors()."</div>";
									}

									if ($data == NULL):
										# code...
									else:
										foreach ($data as $r):
											$hidden = array('id' => $r->id_agen);
										echo form_open('admin/updateunitusaha/'.$r->id_agen, array('class'=>'form-horizontal form-label-left', 'id'=>'formData'), $hidden);
								?>
                                        	<span class="section"><i class="fa fa-info-circle"></i> Edit Data Unit Usaha</span>

											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_agen">Nama Unit Usaha <span class="required text-danger">*</span></label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<input type="text" id="nama_agen" name="nama_agen" value="<?=ucwords($r->nama_agen);?>" required="required" class="form-control col-md-7 col-xs-12" placeholder="Contoh: Egy Maulana">
												</div>
											</div>

											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_usaha">Jenis Usaha <span class="required text-danger">*</span></label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<input type="text" id="jenis_usaha" name="jenis_usaha" value="<?=ucwords($r->jenis_usaha);?>" required="required" class="form-control col-md-7 col-xs-12" placeholder="Contoh: Kuningan">
												</div>
											</div>

											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat <span class="required text-danger">*</span></label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<textarea id="alamat" required="required" name="alamat" class="form-control col-md-7 col-xs-12"><?=ucwords($r->alamat);?></textarea>
												</div>
											</div>

											<div class="ln_solid"></div>

											<div class="form-group">
												<div class="col-md-6 col-md-offset-3">
													<button id="send" name="btn-send" type="submit" class="btn btn-success" value="btn-value">Submit</button>
												</div>
											</div>
								<?php
										echo form_close();
									endforeach;
									endif;
								?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

<?php $this->load->view('template/footer'); ?>
<?php $this->load->view('template/javascript'); ?>

<script src="<?=base_url('assets/gentelella/vendors/validator/validator.js');?>"></script>
<?php if ($this->session->flashdata('error')): ?>
<script type="text/javascript">
	swal('Oops!', "<?=$this->session->flashdata('error');?>", 'error');
</script>
<?php
endif;
$this->load->view('template/end');
?>
