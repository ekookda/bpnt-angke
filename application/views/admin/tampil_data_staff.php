<?php $this->load->view('template/header'); ?>
<!-- CSS DataTables Bootstrap -->
<link rel="stylesheet" href="<?=base_url('assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css');?>">

<?php $this->load->view('template/main'); ?>
<?php $this->load->view('template/sidebar-menu'); ?>
<?php $this->load->view('template/top-navigation'); ?>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="container-fluid">
			<div class="page-title">
				<div class="title_left">
					<h5></h5>
				</div>

				<div class="title_right">
					<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
						<div class="">

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2><i class="fa fa-info-circle"></i> Informasi Data Admin<small></small></h2>
					<div class="nav navbar-right panel_toolbox">

					</div>

					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<p class="text-muted font-13 m-b-30">

					</p>

					<div class="table-responsive">
						<table id="datatable-responsive" class="table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-center" style="width:10px;">No</th>
									<th class="text-center">Nama Lengkap</th>
									<th class="text-center">Tempat Lahir</th>
									<th class="text-center">Tanggal Lahir</th>
									<th class="text-center">Jenis Kelamin</th>
									<th class="text-center">Nomor HP</th>
									<th class="text-center">Email</th>
								</tr>
							</thead>
							<tbody>
							<?php
								if (!$row):
									// Tidak ada data
								else:
									$no = 1;
									foreach ($row->result() as $r):
							?>
										<tr>
											<!-- <td><?=$r->id_ktp;?></td> -->
											<td class="text-center"><?=$no++;?></td>
											<td><?=ucwords($r->nama_lengkap);?></td>
											<td class="text-center"><?=ucwords($r->tempat_lahir);?></td>
											<?php $date = date("d F Y", strtotime($r->tanggal_lahir)); ?>
											<td class="text-center"><?=$date;?></td>
											<td class="text-center"><?=ucfirst($r->jenis_kelamin);?></td>
											<td class="text-center"><?=$r->nomor_telepon;?></td>
											<td class="text-center"><?=strtolower($r->email);?></td>
										</tr>
							<?php
									endforeach;
								endif;
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->

<?php $this->load->view('template/footer'); ?>
<?php $this->load->view('template/javascript'); ?>

<?php if ($this->session->flashdata('error')): ?>
<script type="text/javascript">
	swal('Oops!', "<?=$this->session->flashdata('error');?>", 'error');
</script>
<?php elseif ($this->session->flashdata('success')): ?>
<script type="text/javascript">
	swal('Berhasil!', "<?=$this->session->flashdata('success');?>", 'success');
</script>
<?php endif; ?>

<?php $this->load->view('template/end'); ?>
