<?php $this->load->view('template/header'); ?>
<!-- CSS DataTables Bootstrap -->
<link rel="stylesheet" href="<?=base_url('assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css');?>">

<?php $this->load->view('template/main'); ?>
<?php $this->load->view('template/sidebar-menu'); ?>
<?php $this->load->view('template/top-navigation'); ?>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="container-fluid">
		<div class="page-title">
			<div class="title_left">
				<h5></h5>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="">

					</div>
				</div>
			</div>
		</div>
		</div>

		<div class="clearfix"></div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2><i class="fa fa-info-circle"></i> Informasi Jadwal Transfer<small></small></h2>
					<div class="nav navbar-right panel_toolbox">
						<a class="btn btn-primary" id="btn-link" href="<?=site_url('admin/inputjadwal');?>"><i class="fa fa-plus-circle"></i> Tambah Data</a>
						<!-- <button class="btn btn-default" type="button" onclick="window.print();"><i class="fa fa-print"></i> Cetak</button> -->
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<p class="text-muted font-13 m-b-30"></p>

					<div class="table-responsive">
						<table id="datatables" class="table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-center" width="10px">No</th>
									<th class="text-center">RT / RW</th>
									<th class="text-center">Hari</th>
									<th class="text-center">Tanggal</th>
									<th class="text-center" style="width:70px;"></th>
								</tr>
							</thead>
							<tbody>
							<?php
							if (empty($row)):
								echo '<div id="no-data"></div>';
							else:
								$no = 1;
								$name_of_the_day = ['minggu', 'senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu'];
								$bulan = [
									1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni',
									7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'
								];

								foreach ($row as $r):
									$hari  = date("w", strtotime($r->tanggal_transfer));
									$day   = date("d", strtotime($r->tanggal_transfer));
									$month = date("n", strtotime($r->tanggal_transfer));
									$year  = date("Y", strtotime($r->tanggal_transfer));
							?>
									<tr>
										<!-- <td><?=$r->id_ktp;?></td> -->
										<td class="text-center"><?=$no++;?></td>
										<td class="text-center"><?=ucwords($r->nomor_rt. "/" . $r->nomor_rw);?></td>
										<td class="text-center"><?=ucwords($name_of_the_day[$hari]);?></td>
										<td class="text-center"><?=ucwords($day. " " .$bulan[$month]. " " .$year);?></td>
										<td class="text-center">
											<a href="<?=site_url('admin/editjadwal/'.$r->id_jadwal);?>" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
											<a id="hapus" class="btn btn-danger btn-xs" href="<?=site_url('admin/hapusjadwal/'.$r->id_jadwal);?>" onclick="return confirm('Apa anda yakin ingin menghapusnya dari sistem?');">
												<i class="fa fa-trash"></i>
											</a>
										</td>
									</tr>
							<?php
								endforeach;
							endif;
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->

<?php $this->load->view('template/footer'); ?>
<?php $this->load->view('template/javascript'); ?>
<!-- Datatables JS -->
<script src="<?=base_url('assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
<script src="<?=base_url('assets/gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js');?>"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatables').DataTable({
			dom: 'Bfrtip',
			buttons: [
				{
					extend: 'print',
					message: 'This print was produced using the Print button for DataTables',
					exportOptions: {
						columns: [ 0, 1, 2, 3 ]
					},
					customize: function ( win ) {
						$(win.document.body)
							.css( 'font-size', '10pt' )
							// .prepend(
							// 	'<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
							// );

						$(win.document.body).find( 'table' )
							.addClass( 'compact' )
							.css( 'font-size', 'inherit' );
					}
				}
			]
		});
	});

	<?php if(empty($row)): ?>
		$('#no-data').html(swal('Info', 'Tidak ada data agen yang tersimpan', 'info'));
	<?php endif; ?>
</script>

<?php if ($this->session->flashdata('error')): ?>
<script type="text/javascript">
	swal('Oops!', "<?=$this->session->flashdata('error');?>", 'error');
</script>
<?php elseif ($this->session->flashdata('success')): ?>
<script type="text/javascript">
	swal('Berhasil!', "<?=$this->session->flashdata('success');?>", 'success');
</script>
<?php endif; ?>

<?php $this->load->view('template/end'); ?>
