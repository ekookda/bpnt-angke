<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/main'); ?>
<?php $this->load->view('template/sidebar-menu'); ?>
<?php $this->load->view('template/top-navigation'); ?>

            <!-- Page Content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left"></div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
									<?= form_open('admin/store_rt', array('class'=>'form-horizontal form-label-left', 'id'=>'formData')); ?>
                                        <span class="section"><i class="fa fa-info-circle"></i> Input Data RT</span>

										<?php
											if (validation_errors()) {
												echo "<div class='alert alert-danger'>".validation_errors()."</div>";
											}
										?>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="username" name="username" value="<?=set_value('username');?>" required="required" class="form-control col-md-7 col-xs-12" placeholder="Contoh: egi.maulana">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="password" id="password" name="password" value="<?=set_value('password');?>" required="required" class="form-control col-md-7 col-xs-12" placeholder="Minimal 8 karakter">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="passconf">Konfirmasi Password <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="password" id="passconf" name="passconf" value="<?=set_value('passconf');?>" required="required" class="form-control col-md-7 col-xs-12" placeholder="Minimal 8 karakter">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_lengkap">Nama Lengkap <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="nama_lengkap" name="nama_lengkap" value="<?=set_value('nama_lengkap');?>" required="required" class="form-control col-md-7 col-xs-12" placeholder="Contoh: Egy Maulana">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tempat_lahir">Tempat Lahir <span class="required text-danger">*</span></label>
                                        	<div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="tempat_lahir" name="tempat_lahir" value="<?=set_value('tempat_lahir');?>" required="required" class="form-control col-md-7 col-xs-12" placeholder="Contoh: Kuningan">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_lahir">Tanggal Lahir <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="date" id="tanggal_lahir" name="tanggal_lahir" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="alamat" required="required" name="alamat" class="form-control col-md-7 col-xs-12"><?=set_value('alamat');?></textarea>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor_rt">RT <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
												<select class="form-control" name="nomor_rt" id="nomor_rt" required>
													<option>-- RT --</option>
													<?php
													for ($i=1; $i<=10; $i++) {
														$no = str_pad($i, 3, '00', STR_PAD_LEFT);
														echo "<option value='".$no."'>$no</option>";
													}
													?>
												</select>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telp">Nomor Telepon <span class="required text-danger">*</span></label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="no_telp" name="no_telp" value="<?=set_value('no_telp');?>" required="required" placeholder="Contoh: 081517406276" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label for="jenis_kelamin" class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
												<label class="radio_inline"><input type="radio" name="jenis_kelamin" value="pria" id="jenis_kelamin"> Pria</label>
												<label class="radio_inline"><input type="radio" name="jenis_kelamin" value="perempuan" id="jenis_kelamin"> Perempuan</label>
                                            </div>
										</div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required text-danger">*</span></label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="email" id="email" name="email" value="<?=set_value('email');?>" required="required" placeholder="Contoh: andre.taulani@gmail.com" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="ln_solid"></div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-3">
                                                <button id="send" name="btn-send" type="submit" class="btn btn-success" value="btn-value">Submit</button>
                                            </div>
                                        </div>
									<?=form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

<?php $this->load->view('template/footer'); ?>
<?php $this->load->view('template/javascript'); ?>

<script src="<?=base_url('assets/gentelella/vendors/validator/validator.js');?>"></script>
<?php if ($this->session->flashdata('error')): ?>
<script type="text/javascript">
	swal('Oops!', "<?=$this->session->flashdata('error');?>", 'error');
</script>
<?php
endif;
$this->load->view('template/end');
?>
