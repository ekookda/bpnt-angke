	<!-- jQuery -->
	<script src="<?=base_url('assets/node_modules/jquery/dist/jquery.min.js');?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<!-- Bootstrap JS -->
	<script src="<?=base_url('assets/node_modules/bootstrap/dist/js/bootstrap.min.js');?>"></script>
	<!-- SweetAlert JS -->
	<script src="<?=base_url('assets/node_modules/sweetalert/dist/sweetalert.min.js');?>"></script>

	<?php if ($this->session->userdata('level') !== NULL): ?>
		<!-- Custom Theme Scripts -->
		<script src="<?=base_url('assets/gentelella/build/js/custom.min.js');?>"></script>
	<?php endif; ?>
