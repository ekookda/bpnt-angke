			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="#" class="site_title"><i class="fa fa-paw"></i> SIM<span class="text-danger">BPNT</span></a>
					</div>

					<div class="clearfix"></div>

					<!-- menu profile quick info -->
					<div class="profile clearfix">
						<div class="profile_pic">
							<img src="<?=base_url();?>assets/gentelella/production/images/user.png" alt="..." class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<?php
							/*
							** Mendapatkan session level untuk dipakai sebagai kondisi ke database berdasarkan level
							*/
								$username = ($this->session->userdata('username') ? $this->session->userdata('username') : FALSE);
								$level = ($this->session->userdata('level') ? $this->session->userdata('level') : FALSE);
								$where = array('username'=>$username);

								switch ($level) {
									case 'staff':
									case 'lurah':
										$qry = $this->db->get_where('pegawai_kelurahan', $where);
										break;
									case 'rtrw':
										$qry = $this->db->get_where('rtrw', $where);
										break;
									default:
										$qry = FALSE;
										break;
								}

								if ($qry->num_rows() == 1 && $qry == TRUE) {
									$result = $qry->result_array();
									$name   = ucwords($result[0]['nama_lengkap']);
									$this->session->set_userdata(array('nama'=>$name));
								} else {
									$name = 'Anonymous';
								}

							?>
							<span>Welcome,</span>
							<h2><?=$name;?></h2>
						</div>
					</div>
					<!-- /menu profile quick info -->

					<br />

					<!-- Sidebar-Menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3>General</h3>
							<ul class="nav side-menu">
								<li>
									<?php $uri = $this->uri->segment(1);?>
									<a href="<?=site_url($uri);?>"><i class="fa fa-home"></i> Beranda </a>
								</li>

								<?php if ($level == 'lurah'): ?>
								<li>
									<a><i class="fa fa-file-o"></i> Lihat Laporan <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="<?=site_url('lurah/laporandatawarga');?>"><i class="fa fa-book"></i> Laporan Data Warga</a></li>
										<li><a href="<?=site_url('lurah/laporandataagen');?>"><i class="fa fa-book"></i> Laporan Data Agen</a></li>
									</ul>
								</li>
								<?php elseif ($level == 'rtrw'): ?>
								<li>
									<a><i class="fa fa-file-o"></i> Lihat Data Warga <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="<?=site_url('rtrw/datawarga');?>"><i class="fa fa-book"></i> Kelola Data Warga</a></li>
									</ul>
								</li>
								<?php elseif ($level == 'staff'): ?>
								<li>
									<a><i class="fa fa-briefcase"></i> Manajemen Data <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="<?=site_url('admin/datart');?>"><i class="fa fa-book"></i> Kelola Data RT</a></li>
										<li><a href="<?=site_url('admin/datawarga');?>"><i class="fa fa-book"></i> Kelola Data Warga</a></li>
										<li><a href="<?=site_url('admin/datastaff');?>"><i class="fa fa-book"></i> Kelola Staff</a></li>
										<li><a href="<?=site_url('admin/datalurah');?>"><i class="fa fa-book"></i> Kelola Lurah</a></li>
										<li><a href="<?=site_url('admin/unitusaha');?>"><i class="fa fa-book"></i> Kelola Unit Usaha</a></li>
									</ul>
								</li>

								<li><a href="<?=site_url('Admin/jadwal_transfer');?>"><i class="fa fa-archive"></i> Jadwal Transfer</a></li>


								<?php endif; ?>

							</ul>
						</div>
					</div> <!-- /.sidebar-menu -->

					<!-- /menu footer buttons -->
					<div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Settings">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="FullScreen">
							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Lock">
							<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Logout" href="<?=site_url('welcome/signout');?>">
							<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
						</a>
					</div> <!-- /menu footer buttons -->
				</div> <!-- /.left_col scroll-view -->
			</div> <!-- /.col-md-3 left_col -->
