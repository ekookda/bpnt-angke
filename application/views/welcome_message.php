<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kelurahan Angke</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url('assets/node_modules/bootstrap/dist/css/bootstrap4.min.css');?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?=base_url('assets/gentelella/build/css/blog-home.css');?>" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    	<div class="container">
        	<a class="navbar-brand" href="#">
				<img src="<?=base_url('assets/gentelella/production/images/logo_dki.png');?>" class="img-fluid" alt="Logo DKI Jakarta" style="width:10%;">
				Kelurahan Angke
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="<?=site_url('welcome');?>">Beranda
							<span class="sr-only">(current)</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?=site_url('welcome/#');?>">Profile</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?=site_url('welcome/informasi');?>">Informasi</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?=site_url('welcome/#');?>">Kontak Kami</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="<?=site_url('welcome/login');?>">Login</a>
					</li>
				</ul>
			</div>
		</div>
    </nav>

    <!-- Page Content -->
    <div class="container">
		<div class="row">

			<!-- Blog Entries Column -->
			<div class="col-md-8">
				<h1 class="my-4"></h1>

				<!-- Banner Image -->
				<div class="card mb-4">
					<img class="card-img-top img-fluid img-thumbnail" src="<?=base_url('assets/gentelella/production/images/banner_angke.jpg');?>" alt="Card image cap">
				</div>

				<!-- Blog Post -->
				<div class="card mb-4">
					<img class="card-img-top img-fluid img-thumbnail" src="<?=base_url('assets/gentelella/production/images/jumantik.jpg');?>" alt="Card image cap">
					<div class="card-body">
						<h2 class="card-title">Kader Jumantik RW 007</h2>
						<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
						<a href="#" class="btn btn-primary">Read More &rarr;</a>
					</div>
					<div class="card-footer text-muted">
						Posted on January 1, 2017 by
						<a href="#">Start</a>
					</div>
				</div>

				<!-- Pagination -->
				<ul class="pagination justify-content-center mb-4">
					<li class="page-item">
						<a class="page-link" href="#">&larr; Older</a>
					</li>
					<li class="page-item disabled">
						<a class="page-link" href="#">Newer &rarr;</a>
					</li>
				</ul>
			</div>

			<!-- Sidebar Widgets Column -->
			<div class="col-md-4">

				<!-- Search Widget -->
				<div class="card my-4">
					<h5 class="card-header">Search</h5>
					<div class="card-body">
						<?=form_open('welcome/index', array('id'=>'form_search'));?>
							<div class="input-group">
								<input type="text" class="form-control" required="" name="nomor_ktp">
								<span class="input-group-btn">
									<button class="btn btn-secondary" type="submit" name="btn-search" id="btn-search">Search</button>
								</span>
							</div>
						<?=form_close();?>

						<?php if (isset($row)): ?>
							<hr>
							<div class="table-responsive">
								<table class="table table-hover table-sm table-responsive">
									<tr>
										<td>Nomor KTP</td>
										<td>:</td>
										<td>sd</td>
									</tr>
									<tr>
										<td>Nama Lengkap</td>
										<td>:</td>
										<td>sd</td>
									</tr>
									<tr>
										<td>Jadwal Transfer</td>
										<td>:</td>
										<td>sd</td>
									</tr>
									<tr>
										<td>Alamat</td>
										<td>:</td>
										<td>sd</td>
									</tr>
									<tr>
										<td>RT</td>
										<td>:</td>
										<td>sd</td>
									</tr>
									<tr>
										<td>Status</td>
										<td>:</td>
										<td>sd</td>
									</tr>
								</table>
							</div>
						<?php endif; ?>
					</div>
				</div>

				<!-- Categories Widget -->
				<div class="card my-4">
					<h5 class="card-header">Categories</h5>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6">
								<ul class="list-unstyled mb-0">
									<li>
										<a href="<?=site_url('welcome/warga');?>">Data Warga </a>
									</li>
									<li>
										<a href="<?=site_url('welcome/jadwal_transfer');?>">Jadwal Transfer</a>
									</li>
									<li>
										<a href="<?=site_url('welcome/data_unit_usaha');?>">Unit Usaha</a>
									</li>
								</ul>
							</div>
							<div class="col-lg-6">
								<ul class="list-unstyled mb-0">
									<li>
										<a href="#"></a>
									</li>
									<li>
										<a href="#"></a>
									</li>
									<li>
										<a href="#"></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<!-- Side Widget -->
				<div class="card my-4">
					<h5 class="card-header">Side Widget</h5>
					<div class="card-body">
						You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
					</div>
				</div>

			</div>

		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright &copy; Kelurahan Angke 2017</p>
		</div>
		<!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url('assets/node_modules/jquery/dist/jquery.min.js');?>"></script>
    <script src="<?=base_url('assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js');?>"></script>
	<!-- SweetAlert -->
    <script src="<?=base_url('assets/node_modules/sweetalert/dist/sweetalert.min.js');?>"></script>

</body>
</html>
