<?php $this->load->view('template/header'); ?>
<!-- CSS DataTables Bootstrap -->
<link rel="stylesheet" href="<?=base_url('assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css');?>">

<?php $this->load->view('template/main'); ?>
<?php $this->load->view('template/sidebar-menu'); ?>
<?php $this->load->view('template/top-navigation'); ?>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="container-fluid">
		<div class="page-title">
			<div class="title_left">
				<h5></h5>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="">

					</div>
				</div>
			</div>
		</div>
		</div>

		<div class="clearfix"></div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2 class="text-info"><i class="fa fa-info-circle"></i> Data Unit Usaha<small></small></h2>
					<div class="nav navbar-right panel_toolbox">
						<button class="btn btn-default" id="print"><i class="fa fa-print"></i> Cetak</button>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<p class="text-muted font-13 m-b-30">

					</p>

					<div id="printTable">
						<div id="heading" style=""></div>
						<div class="table-responsive">
							<table id="datatable-responsive" class="table table-striped table-bordered" cellspacing="0" width="100%" border="1px solid black">
								<thead>
									<tr>
										<th class="text-center">No</th>
										<th class="text-center">ID Agen</th>
										<th class="text-center">Nama Agen</th>
										<th class="text-center">Jenis Usaha</th>
										<th class="text-center">Alamat</th>
									</tr>
								</thead>
								<tbody>
								<?php
									if (!$data_tbagen || $data_tbagen == NULL):
										// Tidak ada data

									else:
										$no = 1;
										foreach ($data_tbagen->result() as $r):
								?>
											<tr>
												<!-- <td><?=$r->id_ktp;?></td> -->
												<td class="text-center"><?=$no++;?></td>
												<td class="text-center"><?=ucwords($r->id_agen);?></td>
												<td class="text-center"><?=ucwords($r->nama_agen);?></td>
												<td class="text-center"><?=ucwords($r->jenis_usaha);?></td>
												<td><?=ucwords($r->alamat);?></td>
											</tr>
								<?php
										endforeach;
									endif;
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->

<?php $this->load->view('template/footer'); ?>
<?php $this->load->view('template/javascript'); ?>

<script type="text/javascript">
var divToPrint = document.getElementById("printTable");

document.getElementById("print").onclick= function(){
	document.getElementById('heading').innerHTML = "<h2>Laporan Data Unit Usaha/Agen</h2><hr>";
	document.getElementById('datatable-responsive').style.border = "1px solid black";

	newWin = window.open("");
	newWin.document.write(divToPrint.outerHTML);
	newWin.print();
	newWin.close();
};
</script>

<?php $this->load->view('template/end'); ?>
