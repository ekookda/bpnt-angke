<?php $this->load->view('template/header'); ?>

<!-- Highchart Style -->
<link href="<?=base_url('assets/node_modules/highcharts/css/highcharts.css')?>" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="<?=base_url('assets/gentelella/build/css/custom.min.css')?>" rel="stylesheet">

<?php $this->load->view('template/main'); ?>
<?php $this->load->view('template/sidebar-menu'); ?>
<?php $this->load->view('template/top-navigation'); ?>

<!-- Page Content -->
<div class="right_col" role="main">
	<!-- top tiles -->
	<?php
		if ($total_warga !== NULL && isset($total_warga)) {
			$total_warga = $total_warga;
		} else {
			$total_warga = '0';
		}

		// Total Data Warga yang Berhak
		if ($total_berhak !== NULL && isset($total_berhak)) {
			$total_berhak = $total_berhak;
		} else {
			$total_berhak = '0';
		}

		// Total Data Warga yang Belum Berhak
		if ($total_belumBerhak !== NULL && isset($total_belumBerhak)) {
			$total_belumBerhak = $total_belumBerhak;
		} else {
			$total_belumBerhak = '0';
		}

		// Total Data Agen/Unit Usaha
		if ($total_agen !== NULL && isset($total_agen)) {
			$total_agen = $total_agen;
		} else {
			$total_agen = '0';
		}

		// Total Data RT
		if ($total_rt !== NULL && isset($total_rt)) {
			$total_rt = $total_rt;
		} else {
			$total_rt = '0';
		}
	?>

	<div class="">
	    <img src="<?=base_url('assets/gentelella/production/images/banner_angke.jpg');?>" class="img-responsive img-thumbnail" style="width:100%; margin-bottom:10px">

		<div class="row top_tiles">
			<!-- Warga yang berhak -->
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-user"></i></div>
					<div class="count"><?=$total_berhak?></div>
					<br>
					<h3>Warga Yang Berhak</h3>
				</div>
			</div>
			<!--  -->
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-user"></i></div>
					<div class="count"><?=$total_belumBerhak?></div>
					<br>
					<h3>Warga Belum Berhak</h3>
				</div>
			</div>
			<!--  -->
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-user"></i></div>
					<div class="count"><?=$total_rt?></div>
					<br>
					<h3>Total RT</h3>
				</div>
			</div>
			<!--  -->
			<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-user"></i></div>
					<div class="count"><?=$total_agen?></div>
					<br>
					<h3>Jumlah Unit Usaha</h3>
				</div>
			</div>

		</div>
	</div>

	<br />
</div> <!-- /page content -->

<?php $this->load->view('template/footer'); ?>
<?php $this->load->view('template/javascript'); ?>

<?php
for ($i=0; $i <= 9; $i++) {
	$value[] = $count[0]['rt_'.$i];
}


// print_r($count);
// $value[] = $count[0]['jumlah'];
?>

<script src="<?=base_url('assets/node_modules/highcharts/highcharts.js');?>" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#report').highcharts({
			chart: {
				type: 'column',
				margin: 75,
				options3d: {
					enabled: false,
					alpha: 10,
					beta: 25,
					depth: 70
				}
			},
			title: {
				text: 'Report Warga Penerima',
				style: {
						fontSize: '18px',
						fontFamily: 'Verdana, sans-serif'
				}
			},
			subtitle: {
				text: 'BPNT Angke',
				style: {
					fontSize: '15px',
					fontFamily: 'Verdana, sans-serif'
				}
			},
			plotOptions: {
				column: {
					depth: 25
				}
			},
			credits: {
				enabled: false
			},
			xAxis: {
				categories: <?php echo json_encode($count);?>
			},
			exporting: {
				enabled: false
			},
			yAxis: {
				title: {
					text: 'Jumlah Penerima'
				},
			},
			tooltip: {
				formatter: function() {
					return 'The value for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
				}
			},
			series: [{
				name: 'Report Data',
				colorByPoint: true,
				data: <?php echo json_encode($value); ?>,
				shadow : true,
				dataLabels: {
					enabled: true,
					color: '#045396',
					align: 'center',
					formatter: function() {
						return Highcharts.numberFormat(this.y, 0);
					}, // one decimal
					y: 0, // 10 pixels down from the top
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			}]
		});
	});
</script>

<?php $this->load->view('template/end'); ?>
