<!DOCTYPE html>
<html>
<head>
	<title>Report Table</title>
	<style type="text/css">
		#outtable
		{
			padding: 20px;
			border:1px solid #e3e3e3;
			width:auto;
			border-radius: 1px;
		}

		.short
		{
			width: 50px;
		}

		.normal
		{
			width: 150px;
		}

		.text-center
		{
			text-align: center;
		}

		table
		{
			border-collapse: collapse;
			font-family: arial;
			color:#5E5B5C;
		}

		thead th
		{
			text-align: left;
			padding: 10px;
		}

		tbody td
		{
			border-top: 1px solid #e3e3e3;
			padding: 10px;
		}

		tbody tr:nth-child(even)
		{
			background: #F6F5FA;
		}

		tbody tr:hover
		{
			background: #EAE9F5
		}
	</style>
</head>
<body>
	<div id="outtable">
		<h2>Report Data Unit Usaha/Agen</h2>
		<hr>
		<table>
			<thead>
				<tr>
					<th class="text-center">#</th>
					<th class="text-center">ID</th>
					<th class="text-center normal">Nama Agen</th>
					<th class="text-center normal">Jenis Usaha</th>
					<th class="text-center normal">Alamat</th>
				</tr>
			</thead>
			<tbody>
				<?php $no=1; ?>
				<?php foreach($agen as $user): ?>
				<tr>
					<td class="text-center"><?php echo $no; ?></td>
					<td class="text-center"><?php echo $user['id_agen']; ?></td>
					<td><?php echo $user['nama_agen']; ?></td>
					<td class="text-center"><?php echo $user['jenis_usaha']; ?></td>
					<td><?php echo $user['alamat']; ?></td>
				</tr>
				<?php $no++; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</body>
</html>
