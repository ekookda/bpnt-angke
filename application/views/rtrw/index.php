<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/main'); ?>
<?php $this->load->view('template/sidebar-menu'); ?>
<?php $this->load->view('template/top-navigation'); ?>

<!-- Page Content -->
<div class="right_col" role="main">
	<!-- top tiles -->
	<?php
		if ($total_warga !== NULL && isset($total_warga)) {
			$total_warga = $total_warga;
		} else {
			$total_warga = '0';
		}

		// Total Data Warga yang Berhak
		if ($total_berhak !== NULL && isset($total_berhak)) {
			$total_berhak = $total_berhak;
		} else {
			$total_berhak = '0';
		}

		// Total Data Warga yang Belum Berhak
		if ($total_belumBerhak !== NULL && isset($total_belumBerhak)) {
			$total_belumBerhak = $total_belumBerhak;
		} else {
			$total_belumBerhak = '0';
		}

		// Total Data Agen/Unit Usaha
		if ($total_agen !== NULL && isset($total_agen)) {
			$total_agen = $total_agen;
		} else {
			$total_agen = '0';
		}

		// Total Data RT
		if ($total_rt !== NULL && isset($total_rt)) {
			$total_rt = $total_rt;
		} else {
			$total_rt = '0';
		}
	?>

	<!-- Graphic -->
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="dashboard_graph">
				<div class="row x_title">
					<div class="col-md-6">
						<h3><?php if ($nomor_rt != '0') echo "RT " . $nomor_rt; ?>
							RW <?= $nomor_rw; ?>
						</h3>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- End Graphic -->

	<br>

	<?php if ($level == 1): ?>
		<div class="">
			<div class="row top_tiles">
				<!-- Warga yang berhak -->
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats">
						<div class="icon"><i class="fa fa-user"></i></div>
						<div class="count"><?=$total_berhak?></div>
						<br>
						<h3>Warga Yang Berhak</h3>
					</div>
				</div>
				<!-- Total Belum Berhak -->
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats">
						<div class="icon"><i class="fa fa-user"></i></div>
						<div class="count"><?=$total_belumBerhak?></div>
						<br>
						<h3>Warga Belum Berhak</h3>
					</div>
				</div>
				<!-- Total Warga -->
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats">
						<div class="icon"><i class="fa fa-user"></i></div>
						<div class="count"><?=$total_warga?></div>
						<br>
						<h3>Total Warga RT <?=$nomor_rt;?></h3>
					</div>
				</div>
				<!-- Total Agen -->
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats">
						<div class="icon"><i class="fa fa-user"></i></div>
						<div class="count"><?=$total_agen?></div>
						<br>
						<h3>Unit Usaha</h3>
					</div>
				</div>
			</div>
		</div>
	<?php elseif ($level == 2): ?>
		<div class="">
			<div class="row top_tiles">
				<!-- Warga yang berhak -->
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats">
						<div class="icon"><i class="fa fa-user"></i></div>
						<div class="count"><?=$total_berhak?></div>
						<br>
						<h3>Warga Yang Berhak</h3>
					</div>
				</div>
				<!-- Total Belum Berhak -->
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats">
						<div class="icon"><i class="fa fa-user"></i></div>
						<div class="count"><?=$total_belumBerhak?></div>
						<br>
						<h3>Warga Belum Berhak</h3>
					</div>
				</div>
				<!-- Total Warga -->
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats">
						<div class="icon"><i class="fa fa-building-o"></i></div>
						<div class="count"><?=$total_rt?></div>
						<br>
						<?php
							if ($level == 2) echo "<h3>Jumlah RT</h3>";
							else echo "<h3>Jumlah Warga</h3>";
						?>
					</div>
				</div>
				<!-- Total Agen -->
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats">
						<div class="icon"><i class="fa fa-user"></i></div>
						<div class="count"><?=$total_agen?></div>
						<br>
						<h3>Unit Usaha</h3>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

</div> <!-- /page content -->

<?php $this->load->view('template/footer'); ?>
<?php $this->load->view('template/javascript'); ?>
<?php $this->load->view('template/end'); ?>
