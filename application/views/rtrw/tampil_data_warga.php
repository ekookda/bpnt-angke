<?php $this->load->view('template/header'); ?>
<!-- CSS DataTables Bootstrap -->
<link rel="stylesheet" href="<?=base_url('assets/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css');?>">

<?php $this->load->view('template/main'); ?>
<?php $this->load->view('template/sidebar-menu'); ?>
<?php $this->load->view('template/top-navigation'); ?>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="container-fluid">
		<div class="page-title">
			<div class="title_left">
				<h5></h5>
			</div>

			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="">

					</div>
				</div>
			</div>
		</div>
		</div>

		<div class="clearfix"></div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<?php if (empty($no_rt)): ?>
						<h2 class="text-info"><i class="fa fa-info-circle"></i> Data Warga RW <?=$no_rw;?><small></small></h2>
					<?php else: ?>
						<h2 class="text-info"><i class="fa fa-info-circle"></i> Data Warga RT <?=$no_rt;?> RW <?=$no_rw;?><small></small></h2>
					<?php endif; ?>
					<div class="nav navbar-right panel_toolbox">
						<a class="btn btn-primary" id="btn-link" href="<?=site_url('rtrw/inputwarga');?>"><i class="fa fa-plus-circle"></i> Tambah Data</a>
						<!-- <button class="btn btn-default" type="button" onclick="window.print();"><i class="fa fa-print"></i> Cetak</button> -->
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<p class="text-muted font-13 m-b-30">

					</p>

					<div class="table-responsive">
						<table id="datatable-responsive" class="table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-center">No KTP</th>
									<th class="text-center">Nama Lengkap</th>
									<th class="text-center">Tempat Lahir</th>
									<th class="text-center">Tanggal Lahir</th>
									<th class="text-center">Jenis Kelamin</th>
									<th class="text-center">Agama</th>
									<th class="text-center">Alamat</th>
									<th class="text-center">RT</th>
									<th class="text-center">Nomor HP</th>
									<th class="text-center">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
								if (!$row || $row == NULL):
									// Tidak ada data

								else:
									$no = 1;
									foreach ($row->result() as $r):
							?>
										<tr>
											<!-- <td><?=$r->id_ktp;?></td> -->
											<td class="text-center"><?=$no++;?></td>
											<td><?=ucwords($r->id_ktp);?></td>
											<td><?=ucwords($r->nama_lengkap);?></td>
											<td class="text-center"><?=ucwords($r->tempat_lahir);?></td>
											<?php $tgl_lahir = date("d F Y", strtotime($r->tanggal_lahir)); ?>
											<td class="text-center"><?=$tgl_lahir;?></td>
											<td class="text-center"><?=ucfirst($r->jenis_kelamin);?></td>
											<td class="text-center"><?=ucfirst($r->agama);?></td>
											<td><?=ucwords($r->alamat);?></td>
											<td class="text-center"><?=$r->rt;?></td>
											<td class="text-center"><?=$r->nomor_telepon;?></td>
											<?php
											switch ($r->status):
												case 1:
													$status = "<span class='label label-success'>Berhak</span>";
													break;
												default:
													$status = "<span class='label label-danger'>Belum</span>";
													break;
											endswitch;
											?>
											<td class="text-center"><?=ucfirst($status);?></td>
											<td class="text-center">
												<a href="<?=site_url('rtrw/editwarga/'.$r->id_ktp);?>" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
												<a id="hapus" class="btn btn-danger btn-xs" href="<?=site_url('rtrw/hapusdatawarga/'.$r->id_ktp);?>" onclick="return confirm('Apa anda yakin ingin menghapusnya dari sistem?');"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
							<?php
									endforeach;
								endif;
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->

<?php $this->load->view('template/footer'); ?>
<?php $this->load->view('template/javascript'); ?>
<!-- Datatables JS -->
<script src="<?=base_url('assets/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
<script src="<?=base_url('assets/gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js');?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable-responsive').DataTable();
	});
</script>

<?php if ($this->session->flashdata('error')): ?>
<script type="text/javascript">
	swal('Oops!', "<?=$this->session->flashdata('error');?>", 'error');
</script>
<?php elseif ($this->session->flashdata('success')): ?>
<script type="text/javascript">
	swal('Berhasil!', "<?=$this->session->flashdata('success');?>", 'success');
</script>
<?php elseif ($row === NULL): ?>
<script type="text/javascript">
	swal('Oops!', "Tidak ada data yang tersimpan", 'warning');
</script>
<?php endif; ?>

<?php $this->load->view('template/end'); ?>
