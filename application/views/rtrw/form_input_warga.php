<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/main'); ?>
<?php $this->load->view('template/sidebar-menu'); ?>
<?php $this->load->view('template/top-navigation'); ?>

            <!-- Page Content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left"></div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_content">
									<?= form_open('rtrw/storewarga', array('class'=>'form-horizontal form-label-left', 'id'=>'formData')); ?>
                                        <span class="section"><i class="fa fa-info-circle"></i> Input Data Warga</span>

										<?php
											if (validation_errors()) {
												echo "<div class='alert alert-danger'>".validation_errors()."</div>";
											}
										?>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_ktp">Nomor KTP <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            	<input id="no_ktp" class="form-control col-md-7 col-xs-12"p value="<?=set_value('no_ktp');?>" maxlength="16" data-validate-length-range="16" data-validate-words="16" name="no_ktp" placeholder="Contoh: 5316044901800001" required="required" type="number">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_lengkap">Nama Lengkap <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="nama_lengkap" name="nama_lengkap" value="<?=set_value('nama_lengkap');?>" required="required" class="form-control col-md-7 col-xs-12" placeholder="Contoh: Egy Maulana">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tempat_lahir">Tempat Lahir <span class="required text-danger">*</span></label>
                                        	<div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="tempat_lahir" name="tempat_lahir" value="<?=set_value('tempat_lahir');?>" required="required" class="form-control col-md-7 col-xs-12" placeholder="Contoh: Kuningan">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_lahir">Tanggal Lahir <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="date" id="tanggal_lahir" name="tanggal_lahir" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="alamat" required="required" name="alamat" class="form-control col-md-7 col-xs-12"><?=set_value('alamat');?></textarea>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor_rt">RT <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
												<select class="form-control" name="nomor_rt" id="nomor_rt" required>
													<option>-- RT --</option>
                                                    <option value="<?=$nomor_rt;?>"><?=$nomor_rt;?></option>
													<?php
													// for ($i=1; $i<=10; $i++) {
													// 	$no = str_pad($i, 3, '00', STR_PAD_LEFT);
													// 	echo "<option value='".$no."'>$no</option>";
													// }
													?>
												</select>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telp">Nomor Telepon <span class="required text-danger">*</span></label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="no_telp" name="no_telp" value="<?=set_value('no_telp');?>" required="required" placeholder="Contoh: 081517406276" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label for="jenis_kelamin" class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
												<label class="radio_inline"><input type="radio" name="jenis_kelamin" value="pria" id="jenis_kelamin"> Pria</label>
												<label class="radio_inline"><input type="radio" name="jenis_kelamin" value="perempuan" id="jenis_kelamin"> Perempuan</label>
                                            </div>
										</div>

                                        <div class="item form-group">
                                            <label for="agama" class="control-label col-md-3 col-sm-3 col-xs-12">Agama <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
												<select name="agama" id="agama" class="form-control" required>
													<option>-- Pilih Salah Satu --</option>
													<?php
														$agama = array(
															'islam' => 'Islam',
															'katolik' => 'Katolik',
															'protestan' => 'Protestan',
															'hindu' => 'Hindu',
															'budha' => 'Budha',
														);
														foreach ($agama as $key => $value) {
															echo "<option value='" . $key . "'>$value</option>";
														}
													?>
												</select>
                                            </div>
                                        </div>

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pengajuan">Acc <span class="required text-danger">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
												<select class="form-control" name="pengajuan" id="pengajuan" required>
													<option value="0">Tidak</option>
													<option value="1">Berhak</option>
												</select>
                                            </div>
                                        </div>

                                        <div class="ln_solid"></div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-3">
                                                <button id="send" name="btn-send" type="submit" class="btn btn-success" value="btn-value">Submit</button>
                                            </div>
                                        </div>
									<?=form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

<?php $this->load->view('template/footer'); ?>
<?php $this->load->view('template/javascript'); ?>

<script src="<?=base_url('assets/gentelella/vendors/validator/validator.js');?>"></script>
<?php if ($this->session->flashdata('error')): ?>
<script type="text/javascript">
	swal('Oops!', "<?=$this->session->flashdata('error');?>", 'error');
</script>

<script type="text/javascript">
    var datefield=document.createElement("input")
    datefield.setAttribute("type", "date")
    if (datefield.type!="date"){ //if browser doesn't support input type="date", load files for jQuery UI Date Picker
        document.write('<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />\n')
        document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"><\/script>\n')
        document.write('<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"><\/script>\n') 
    }
</script>
 
<script>
if (datefield.type!="date"){ //if browser doesn't support input type="date", initialize date picker widget:
    jQuery(function($){ //on document.ready
        $('#tanggal_lahir').datepicker();
    })
}
</script>

<?php
endif;
$this->load->view('template/end');
?>
