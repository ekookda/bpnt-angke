<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Faker\Factory;

class Rtrw extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		// inisiasi Faker
		// $this->faker = Faker\Factory::create('id_ID');
		$this->load->model('m_admin');
	}

	public function isLoggedIn()
	{
		$logged_in = $this->session->userdata('logged_in');
		$level     = $this->session->userdata('level');

		if ($level != 'rtrw' || $logged_in == FALSE)
		{
			$array_items = array('level', 'logged_in');
			$this->session->unset_userdata($array_items);

			$this->session->set_flashdata('error', 'Anda belum melakukan login');
			redirect(site_url('welcome/index'));
		}
	}

	// method menghitung jumlah data suatu table di database
	public function get_count_data($table)
	{
		$query = $this->m_admin->show_table($table);
		$total = $query->num_rows();

		return $total;
	}

	// method menghitung jumlah data dengan kondisi suatu table di database
	public function get_count_where($table, $where)
	{
		$query = $this->m_admin->get_where($table, $where);
		$total = $query->num_rows();

		return $total;
	}

	public function get_true($table)
	{
		$this->db->where(array('status'=>'1'));
		$query = $this->db->get_where($table);

		return $query;
	}

	public function count_rt()
	{
		$get = $this->m_admin->count_rt();

		return $get;
	}

	public function index()
	{
		$username_namaRt = $this->session->userdata('username');
		$where           = array('username' => $username_namaRt);

		// Mendapatkan nomor RT berdasarkan username ketua RT
		$get = $this->m_admin->get_where('rtrw', $where);

		foreach ($get->result() as $r)
		{
			$nomorRt = $r->rt;
			$nomorRw = $r->rw;
			$level   = $r->level;
		}

		$data['title']    = 'Administrator';
		$data['nomor_rt'] = $nomorRt;
		$data['nomor_rw'] = $nomorRw;
		$data['level']    = $level;

		if ($level == 1) // Jika yang login adalah ketua RT
		{
			$data['total_warga']       = $this->get_count_where('warga', array('rt' => $nomorRt, 'rw' => $nomorRw));
			$data['total_berhak']      = $this->get_count_where('warga', array('status' => '1', 'rt' => $nomorRt, 'rw' => $nomorRw));
			$data['total_belumBerhak'] = $this->get_count_where('warga', array('status' => '0', 'rt' => $nomorRt, 'rw' => $nomorRw));
			$data['total_agen']        = $this->get_count_data('agen');
			$data['total_rt']          = $this->get_count_where('warga', array('rt' => $nomorRt, 'rw' => $nomorRw));
		}
		elseif ($level == 2) // Jika yang login adalah ketua RW
		{
			// jumlah RT
			$data['total_warga']       = $this->get_count_where('warga', array('rw' => $nomorRw));
			$data['total_berhak']      = $this->get_count_where('warga', array('status' => '1', 'rw' => $nomorRw));
			$data['total_belumBerhak'] = $this->get_count_where('warga', array('status' => '0', 'rw' => $nomorRw));
			$data['total_agen']        = $this->get_count_data('agen');
			$data['total_rt']          = $this->get_count_where('rtrw', array('rw' => $nomorRw, 'rt!=' => '0'));
		}

		$this->load->view('rtrw/index', $data);
	}

	/* Membuat dummy data warga dengan Faker */
	public function seed()
	{
		$agama = array('islam', 'katolik', 'protestan', 'hindu', 'budha');
		$rt    = array('001', '002', '003', '004', '005', '006', '007', '008', '009', '010');

		for ($i = 2; $i < 3; $i++)
		{
			for ($j=1; $j <= 100; $j++)
			{
				$data = array(
					'id_ktp'        => $this->faker->creditCardNumber,
					'nama_lengkap'  => $this->faker->name,
					'tempat_lahir'  => $this->faker->city,
					'tanggal_lahir' => $this->faker->dateTimeThisCentury->format('Y-m-d'),
					'alamat'        => $this->faker->address,
					'rt'            => $rt[array_rand($rt)],
					'rw'            => str_pad($i, 3, '00', STR_PAD_LEFT),
					'nomor_telepon' => preg_replace('/[^0-9]/', '', $this->faker->phoneNumber),
					'jenis_kelamin' => rand(0, 1) ? 'pria' : 'perempuan',
					'agama'         => $agama[array_rand($agama)],
					'status'        => rand(0, 1) ? '1' : '0'
				);
				$insert = $this->m_admin->insert_table('warga', $data);
			}
		}
		if ($insert == TRUE) echo "Berhasil";
	}

	/* Membuat dummy data ketua RT dengan Faker */
	public function seed_rt()
	{
		for ($i = 1; $i < 10; $i++)
		{
			$signup = array(
				'username' => $this->faker->userName,
				'password' => password_hash('12345678', PASSWORD_DEFAULT),
				'level'    => 'rtrw',
			);

			if (in_array($signup['username'], $signup))
			{
				$sign = $this->m_admin->insert_table('login', $signup);
				$rt    = array('002', '003', '004', '005', '006', '007', '008', '009', '010');
				$data = array(
					'username'		=> $signup['username'],
					'nama_lengkap'  => $this->faker->name,
					'tempat_lahir'  => $this->faker->city,
					'tanggal_lahir' => $this->faker->dateTimeThisCentury->format('Y-m-d'),
					'jenis_kelamin' => rand(0, 1) ? 'pria' : 'perempuan',
					'nomor_telepon' => preg_replace('/[^0-9]/', '', $this->faker->phoneNumber),
					'email'         => $this->faker->freeEmail,
					'alamat'        => $this->faker->address,
					'rt'            => "00" . $i,
					'rw'			=> '002',
					'level'			=> '1'
				);
				$insert = $this->m_admin->insert_table('rtrw', $data);
			}

		}
		if ($insert == TRUE) echo "Berhasil";
	}

	/* Membuat dummy data ketua RT dengan Faker */
	public function seed_rw()
	{
		for ($i = 1; $i < 3; $i++)
		{
			$signup = array(
				'username' => $this->faker->userName,
				'password' => password_hash('12345678', PASSWORD_DEFAULT),
				'level'    => 'rtrw',
			);

			if (in_array($signup['username'], $signup))
			{
				$sign = $this->m_admin->insert_table('login', $signup);
				$rw    = array('001', '002');
				$data = array(
					'username'		=> $signup['username'],
					'nama_lengkap'  => $this->faker->name,
					'tempat_lahir'  => $this->faker->city,
					'tanggal_lahir' => $this->faker->dateTimeThisCentury->format('Y-m-d'),
					'jenis_kelamin' => rand(0, 1) ? 'pria' : 'perempuan',
					'nomor_telepon' => preg_replace('/[^0-9]/', '', $this->faker->phoneNumber),
					'email'         => $this->faker->freeEmail,
					'alamat'        => $this->faker->address,
					'nomor_rw'      => "00" . $i
				);
				$insert = $this->m_admin->insert_table('tbl_rw', $data);
			}

		}
		if ($insert == TRUE) echo "Berhasil";
	}

	/*
	*? CRUD Kelola Data Warga by RTRW
	* table: warga
	*! rtrw hanya bisa melihat laporan data warga yang berhak
	*/
	public function datawarga()
	{
		$data = array();

		$username = $this->session->userdata('username');
		$db_rt    = $this->m_admin->get_where('rtrw', array('username' => $username));
		$result   = $db_rt->result();
		$no_rt    = $result[0]->rt;
		$no_rw    = $result[0]->rw;
		if ($no_rt == 0)
		{
			$db_warga = $this->m_admin->get_where('warga', array('rw'=>$no_rw));
		}
		else
		{
			$db_warga = $this->m_admin->get_where('warga', array('rt' => $no_rt, 'rw'=>$no_rw));
		}

		if ($db_warga->num_rows() > 0)
		{
			$data['row']   = $db_warga;
			$data['no_rt'] = $no_rt;
			$data['no_rw'] = $no_rw;
		}
		else
		{
			$data['row']   = NULL;
			$data['no_rt'] = $no_rt;
			$data['no_rw'] = $no_rw;
		}

		$data['title'] = 'Administrator';
		$this->load->view('rtrw/tampil_data_warga', $data);
	}

	public function inputwarga()
	{
		$sess   = $this->session->userdata('username');
		$where  = array('username'=>$sess);
		$query  = $this->m_admin->get_where('rtrw', $where);
		$result = $query->result();

		foreach ($result as $r)
		{
			$nomor_rt = $r->rt;
		}

		$data['title']    = 'Administrator';
		$data['nomor_rt'] = $nomor_rt;

		$this->load->view('rtrw/form_input_warga', $data);
	}

	public function storewarga()
	{
		$ruleIDKtp = $this->form_validation->set_rules(
			'no_ktp', '<b>Nomor KTP</b>',
			'trim|required|max_length[16]|numeric|is_unique[warga.id_ktp]',
				array('is_unique' => 'Nomor KTP sudah pernah tersimpan')
		);

		$formRule = array(
			array(
				'field' => 'nama_lengkap',
				'label' => '<b>Nama Lengkap</b>',
				'rules' => 'required|max_length[30]'
			),
			array(
				'field' => 'tempat_lahir',
				'label' => '<b>Tempat Lahir</b>',
				'rules' => 'required|max_length[20]'
			),
			array(
				'field' => 'tanggal_lahir',
				'label' => '<b>Tanggal Lahir</b>',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'alamat',
				'label' => '<b>Alamat</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'nomor_rt',
				'label' => '<b>Nomor RT</b>',
				'rules' => 'trim|required|max_length[3]|numeric'
			),
			array(
				'field' => 'no_telp',
				'label' => '<b>Nomor Telepon</b>',
				'rules' => 'trim|required|max_length[13]|numeric'
			),
			array(
				'field' => 'jenis_kelamin',
				'label' => '<b>Jenis Kelamin</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'agama',
				'label' => '<b>Agama</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'pengajuan',
				'label' => '<b>Pengajuan</b>',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($formRule);

		if ($this->form_validation->run() === FALSE)
		{
			$this->session->set_flashdata('error', 'Silahkan cek kembali!');
			$this->inputwarga();
		}
		else
		{
			// lolos validasi
			$inputdatawarga = array(
				'id_ktp'        => $this->input->post('no_ktp'),
				'nama_lengkap'  => $this->input->post('nama_lengkap'),
				'tempat_lahir'  => $this->input->post('tempat_lahir'),
				'tanggal_lahir' => $this->input->post('tanggal_lahir'),
				'alamat'        => $this->input->post('alamat'),
				'rt'            => $this->input->post('nomor_rt'),
				'nomor_telepon' => $this->input->post('no_telp'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'agama'         => $this->input->post('agama'),
				'status'        => $this->input->post('pengajuan')
			);
			$insert = $this->m_admin->insert_table('warga', $inputdatawarga);

			if ($insert)
			{
				$this->session->set_flashdata('success', 'Data berhasil disimpan!');
				redirect(site_url('rtrw/datawarga'));
			}
			else
			{
				$this->session->set_flashdata('error', 'Data Gagal disimpan!');
				$this->inputwarga();
			}
		}
	}

	public function editwarga($id_ktp)
	{
		$where         = array('id_ktp' => $id_ktp);
		$data['title'] = 'Administrator';
		$query         = $this->m_admin->get_where('warga', $where);

		if ($query->num_rows() == 1)
		{
			$data['data'] = $query->result();
		}
		else
		{
			$data['data'] = NULL;
		}

		$this->load->view('rtrw/form_edit_warga', $data);
	}

	public function updatewarga()
	{
		$this->form_validation->set_rules('id', 'ID KTP', 'trim|required|max_length[16]|numeric');
		$id = $this->input->post('id');

		$formRule = array(
			array(
				'field' => 'no_ktp',
				'label' => '<b>Nomor KTP</b>',
				'rules' => 'trim|required|max_length[16]|numeric'
			),
			array(
				'field' => 'nama_lengkap',
				'label' => '<b>Nama Lengkap</b>',
				'rules' => 'required|max_length[30]'
			),
			array(
				'field' => 'tempat_lahir',
				'label' => '<b>Tempat Lahir</b>',
				'rules' => 'required|max_length[20]'
			),
			array(
				'field' => 'tanggal_lahir',
				'label' => '<b>Tanggal Lahir</b>',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'alamat',
				'label' => '<b>Alamat</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'nomor_rt',
				'label' => '<b>Nomor RT</b>',
				'rules' => 'trim|required|max_length[3]|numeric'
			),
			array(
				'field' => 'no_telp',
				'label' => '<b>Nomor Telepon</b>',
				'rules' => 'trim|required|max_length[13]|numeric'
			),
			array(
				'field' => 'jenis_kelamin',
				'label' => '<b>Jenis Kelamin</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'agama',
				'label' => '<b>Agama</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'pengajuan',
				'label' => '<b>Pengajuan</b>',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($formRule);

		if ($this->form_validation->run() === FALSE)
		{
			$this->session->set_flashdata('error', 'Silahkan cek kembali!');
			$this->editwarga($id);
		}
		else
		{
			// lolos validasi
			$id = $this->input->post('id');

			$datawarga = array(
				'id_ktp'        => $this->input->post('no_ktp'),
				'nama_lengkap'  => $this->input->post('nama_lengkap'),
				'tempat_lahir'  => $this->input->post('tempat_lahir'),
				'tanggal_lahir' => $this->input->post('tanggal_lahir'),
				'alamat'        => $this->input->post('alamat'),
				'rt'            => $this->input->post('nomor_rt'),
				'nomor_telepon' => $this->input->post('no_telp'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'agama'         => $this->input->post('agama'),
				'status'        => $this->input->post('pengajuan')
			);

			$where  = array('id_ktp' => $id);
			$update = $this->m_admin->update('warga', $datawarga, $where);

			if ($update)
			{
				$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
				redirect(site_url('rtrw/datawarga'));
			}
			else
			{
				$this->session->set_flashdata('error', 'Data Gagal diperbarui!');
				$this->editwarga($id);
			}
		}
	}

	public function hapusdatawarga($id_ktp)
	{
		$where  = array('id_ktp' => $id_ktp);
		$delete = $this->m_admin->delete('warga', $where);

		if ($delete)
		{
			$this->session->set_flashdata('success', 'Data berhasil dihapus!');
			redirect(site_url('rtrw/datawarga'));
		}
		else
		{
			$this->session->set_flashdata('error', 'Data Gagal dihapus!');
			$this->datawarga();
		}
	}

}
