<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_login');
	}

	public function index()
	{
		if ($this->input->post('nomor_ktp')):
			$rules = array(
				array(
					'field' => 'nomor_ktp',
					'label' => 'Nomor KTP',
					'rules' => 'trim|required|min_length[5]|max_length[16]|numeric'
				)
			);
			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() === FALSE) {
				$this->session->set_flashdata('warning', 'Data tidak ditemukan');
				redirect(site_url('welcome'),'refresh');
			} else {
				$id = $this->input->post('nomor_ktp');
				$this->load->model('m_admin');
				$db = $this->m_admin->get_where('warga', array('id_ktp'=>$id));

				if ($db->num_rows() == 1) {
					$data['row'] = $db->result();
					$this->load->view('welcome_message', $data);
				} else {
					$data['row'] = NULL;
					$this->session->set_flashdata('null', 'Nomor KTP Yang dimasukkan tidak ada');
					redirect('welcome/index','refresh');
				}
			}
		else:
			$this->load->view('welcome_message');
		endif;


	}

	public function login()
	{
		$this->load->view('login');
	}

	public function validation()
	{
		// form_validation
		if ($this->input->post('btn-submit')) {
			$config = array(
				array(
					'field' => 'username',
					'label' => 'Username',
					'rules' => 'trim|required|max_length[20]'
				),
				array(
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'trim|required|min_length[8]'
				)
			);
			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() === FALSE) {
				$this->login();
			} else {
				$username = $this->input->post('username');
				$password = $this->input->post('password');

				$where = array('username'=>$username);
				$db = $this->m_login->signin($where);

				if ($db->num_rows() == 1) {
					foreach ($db->result() as $data) {
						$hash = $data->password;
						if (password_verify($password, $hash) === TRUE) {
							$levels = $data->level;
							// Simpan Session
							$sessData = array(
								'username' => $data->username,
								'name' => preg_replace("([0-9!@#$%^&*()_+`~<>?:\"'.,])", " ", $data->username),
								'level' => $levels,
								'logged_in' => TRUE
							);
							$this->session->set_userdata($sessData);

							// redirect sesuai level
							if ($levels == 'staff')
							{
								$this->session->set_flashdata('success', 'Anda telah berhasil login sebagai ' . $levels);
								redirect(site_url('admin'));
							}
							elseif ($levels == 'lurah')
							{
								$this->session->set_flashdata('success', 'Anda telah berhasil login sebagai ' . $levels);
								redirect(site_url('lurah'));
							}
							elseif ($levels == 'rtrw')
							{
								$this->session->set_flashdata('success', 'Anda telah berhasil login sebagai ' . $levels);
								redirect(site_url('rtrw'));
							}
							else
							{
								$this->session->set_flashdata('error', 'Username belum terdaftar');
								$this->login();
								// redirect(site_url('welcome'));
							}
						} else {
							$this->session->set_flashdata('error', 'Password tidak cocok');
							$this->login();
							// redirect(site_url('welcome'));
						}
					}
				} else {
					$this->session->set_flashdata('error', 'Username belum terdaftar');
					$this->login();
					// redirect(site_url('welcome'));
				}
			}
		} else {
			$this->session->set_flashdata('error', 'Anda belum login!');
			redirect(site_url('welcome/login'));
		}
	}

	public function signout()
	{
		// $this->session->sess_destroy();
		$sess_unset = array('username', 'name', 'level', 'logged_in', 'no_rt');
		$this->session->unset_userdata($sess_unset);
		$this->session->set_flashdata('success', 'Anda telah melakukan logout');
		redirect(site_url('welcome/login'));
	}

	public function warga()
	{
		$data['title'] = 'Data Warga';

		$this->load->view('warga/data_warga', $data);
	}

	public function tampil_data_warga()
	{
		$data['title'] = 'Data Warga';
		$this->load->model('m_admin');
		$where = array('status'=>'1');
		$query = $this->m_admin->get_where('warga', $where, 'rw, rt');

		if ($query->num_rows() > 0) {
			$data['data_tbwarga'] = $query;
		}

		$this->load->view('warga/tampil_data_warga', $data);
	}

	public function jadwal_transfer()
	{
		$data['title'] = 'Jadwal Transfer';

		$this->load->view('warga/data_transfer', $data);
	}

	public function tampil_jadwal_transfer()
	{
		$data['title'] = 'Jadwal Transfer';
		$this->load->model('m_admin');
		$where = array('status'=>'1');
		$query = $this->m_admin->show_table('transfer', 'nomor_rt, tanggal_transfer');

		if ($query->num_rows() > 0)
		{
			$data['data_tbtransfer'] = $query;
		}
		else
		{
			$data['data_tbtransfer'] = NULL;
		}

		$this->load->view('warga/tampil_data_transfer', $data);
	}

	public function data_unit_usaha()
	{
		$data['title'] = 'Unit Usaha';
		$this->load->view('warga/data_unit_usaha', $data);
	}

	public function tampil_unit_usaha()
	{
		$data['title'] = 'Unit Usaha';
		$this->load->model('m_admin');
		$query = $this->m_admin->show_table('agen', 'id_agen');
		if ($query->num_rows() > 0) {
			$data['data_tbagen'] = $query;
		} else {
			$data['data_tbagen'] = NULL;
		}

		$this->load->view('warga/tampil_data_unit_usaha', $data);
	}

}
