<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->isLoggedIn();
		$this->load->model('m_admin');
	}

	public function isLoggedIn()
	{
		$logged_in = $this->session->userdata('logged_in');
		$level     = $this->session->userdata('level');
		if ($level != 'staff' || $logged_in === FALSE) {
			$unset = ['level', 'username', 'logged_in'];
			$this->session->unset_userdata($unset);

			$this->session->set_flashdata('error', 'Anda belum melakukan login');

			redirect(site_url('welcome/index'));
		}
	}

	// method menghitung jumlah data suatu table di database
	public function get_count_data($table)
	{
		$query = $this->m_admin->show_table($table);
		$total = $query->num_rows();

		return $total;
	}

	// method menghitung jumlah data suatu table di database
	public function get_count_data_rt($table)
	{
		$query = $this->m_admin->show_table($table);
		$total = $query->num_rows();

		$result = $query->result_array();

		return $result;
	}

	// method menghitung jumlah data dengan kondisi suatu table di database
	public function get_count_where($table, $where)
	{
		$query = $this->m_admin->get_where($table, $where);
		$total = $query->num_rows();

		return $total;
	}

	public function get_true($table)
	{
		$this->db->where(array('status'=>'1'));
		$query = $this->db->get_where($table);

		return $query;
	}

	public function count_rt()
	{
		$get = $this->m_admin->count_rt();

		return $get;
	}

	public function filterArray($value){
		return ($value == 2);
	}

	public function index()
	{
		$data['title']             = 'Administrator';
		$data['total_warga']       = $this->get_count_data('warga');
		$data['total_berhak']      = $this->get_count_where('warga', array('status' => '1'));
		$data['total_belumBerhak'] = $this->get_count_where('warga', array('status' => '0'));
		$data['total_agen']        = $this->get_count_data('agen');
		// $data['total_rt']          = $this->get_count_data('rtrw');
		$data['count']             = $this->count_rt()->result_array();

		$result = $this->get_count_data_rt('rtrw'); // jumlah row di table rtrw
		$res_array = 0;

		for ($i=0; $i < (count($result)-1); $i++)
		{
			if (!empty($result[$i]['rt']))
			{
				$res_array = $i;
			}
		}
		$data['total_rt'] = $res_array;
		$this->load->view('admin/index', $data);
	}

	/*
	*? CRUD Kelola Data Warga by RTRW
	* table: warga
	*! Admin hanya bisa melihat laporan data warga yang berhak
	*/
	public function datawarga()
	{
		$data = array();

		// $db = $this->m_admin->get_where('warga', array('status' => '1'), 'rt');
		$db = $this->m_admin->show_table('warga', 'rt');
		if ($db->num_rows() > 0)
		{
			$data['row'] = $db;
		}
		else
		{
			$data['row'] = NULL;
		}

		$data['title'] = 'Data Warga';
		$this->load->view('admin/tampil_data_warga', $data);
	}

	/*
	* CRUD Kelola Data RT by Admin
	* table: rtrw
	*/
	public function datart()
	{
		$data = array();
		$where = array('rt !='=>'0');

		$db = $this->m_admin->get_where('rtrw', $where, 'rw, rt');
		if ($db->num_rows() > 0)
		{
			$data['row'] = $db;
		}

		$data['title'] = 'Data RT';
		$this->load->view('admin/tampil_data_rtrw', $data);
	}

	public function input_rt()
	{
		$data['title'] = 'Administrator';
		$this->load->view('admin/form_input_rtrw', $data);
	}

	public function store_rt()
	{
		$formRule = array(
			array(
				'field' => 'username',
				'label' => '<b>Username</b>',
				'rules' => 'required|min_length[5]|max_length[20]|is_unique[login.username]'
			),
			array(
				'field' => 'password',
				'label' => '<b>Password</b>',
				'rules' => 'trim|required|min_length[8]'
			),
			array(
				'field' => 'passconf',
				'label' => '<b>Konfirmasi Password</b>',
				'rules' => 'required|matches[password]'
			),
			array(
				'field' => 'nama_lengkap',
				'label' => '<b>Nama Lengkap</b>',
				'rules' => 'required|max_length[30]'
			),
			array(
				'field' => 'tempat_lahir',
				'label' => '<b>Tempat Lahir</b>',
				'rules' => 'required|max_length[20]'
			),
			array(
				'field' => 'tanggal_lahir',
				'label' => '<b>Tanggal Lahir</b>',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'alamat',
				'label' => '<b>Alamat</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'nomor_rt',
				'label' => '<b>Nomor RT</b>',
				'rules' => 'trim|required|max_length[3]|numeric'
			),
			array(
				'field' => 'no_telp',
				'label' => '<b>Nomor Telepon</b>',
				'rules' => 'trim|required|max_length[13]|numeric'
			),
			array(
				'field' => 'jenis_kelamin',
				'label' => '<b>Jenis Kelamin</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'email',
				'label' => '<b>Email</b>',
				'rules' => 'trim|required|valid_email'
			)
		);
		$this->form_validation->set_rules($formRule);

		if ($this->form_validation->run() === FALSE)
		{
			$this->session->set_flashdata('error', 'Silahkan cek kembali!');
			$this->input_rt();
		}
		else
		{
			// lolos validasi
			$login = array(
				'username' => strtolower($this->input->post('username')),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'level'    => 'rtrw'
			);

			$insert_login = $this->m_admin->insert_table('login', $login);

			if ($insert_login === TRUE)
			{
				$input_data_rt = array(
					'username'      => $login['username'],
					'nama_lengkap'  => ucwords($this->input->post('nama_lengkap')),
					'tempat_lahir'  => ucwords($this->input->post('tempat_lahir')),
					'tanggal_lahir' => $this->input->post('tanggal_lahir'),
					'alamat'        => ucwords($this->input->post('alamat')),
					'rt'            => $this->input->post('nomor_rt'),
					'nomor_telepon' => $this->input->post('no_telp'),
					'jenis_kelamin' => $this->input->post('jenis_kelamin'),
					'email'         => strtolower($this->input->post('email'))
				);
				$insert = $this->m_admin->insert_table('rtrw', $input_data_rt);

				if ($insert)
				{
					$this->session->set_flashdata('success', 'Data berhasil disimpan!');
					redirect(site_url('admin/datart'));
				}
				else
				{
					$this->session->set_flashdata('error', 'Data Gagal disimpan!');
					$this->input_rt();
				}
			}
		}
	}

	public function edit_data_rt($id_rt)
	{
		$where = array('id_rtrw'=>$id_rt);
		$data['title'] = 'Administrator';
		$query = $this->m_admin->get_where('rtrw', $where);

		if ($query->num_rows() == 1) {
			$data['data'] = $query->result();
		} else {
			$data['data'] = NULL;
		}
		$this->load->view('admin/form_edit_rtrw', $data);
	}

	public function update_data_rt()
	{
		$this->form_validation->set_rules('id', 'ID RT', 'trim|required|max_length[3]|numeric');
		$id_rtrw = $this->input->post('id');

		$formRule = array(
			array(
				'field' => 'nama_lengkap',
				'label' => '<b>Nama Lengkap</b>',
				'rules' => 'required|max_length[30]'
			),
			array(
				'field' => 'tempat_lahir',
				'label' => '<b>Tempat Lahir</b>',
				'rules' => 'required|max_length[20]'
			),
			array(
				'field' => 'tanggal_lahir',
				'label' => '<b>Tanggal Lahir</b>',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'alamat',
				'label' => '<b>Alamat</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'nomor_rt',
				'label' => '<b>Nomor RT</b>',
				'rules' => 'trim|required|max_length[3]|numeric'
			),
			array(
				'field' => 'no_telp',
				'label' => '<b>Nomor Telepon</b>',
				'rules' => 'trim|required|max_length[13]|numeric'
			),
			array(
				'field' => 'jenis_kelamin',
				'label' => '<b>Jenis Kelamin</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'email',
				'label' => '<b>Email</b>',
				'rules' => 'trim|required|valid_email'
			)
		);
		$this->form_validation->set_rules($formRule);

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('error', 'Silahkan cek kembali!');
			$this->edit_data_rt($id_rtrw);
		} else {
			// lolos validasi
			$data_rtrw = array(
				'nama_lengkap'  => $this->input->post('nama_lengkap'),
				'tempat_lahir'  => $this->input->post('tempat_lahir'),
				'tanggal_lahir' => $this->input->post('tanggal_lahir'),
				'alamat'        => $this->input->post('alamat'),
				'rt'            => $this->input->post('nomor_rt'),
				'nomor_telepon' => $this->input->post('no_telp'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'email'         => $this->input->post('email')
			);
			$where = array('id_rtrw'=>$id_rtrw);
			$update = $this->m_admin->update('rtrw', $data_rtrw, $where);

			if ($update) {
				$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
				redirect(site_url('admin/datart'));
			} else {
				$this->session->set_flashdata('error', 'Data Gagal diperbarui!');
				$this->edit_data_rt($id);
			}
		}
	}

	public function hapus_data_rt($id_rt)
	{
		$where = array('id_rtrw'=>$id_rt);
		$delete = $this->m_admin->delete('rtrw', $where);

		if ($delete) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus!');
			redirect(site_url('admin/datart'));
		} else {
			$this->session->set_flashdata('error', 'Data Gagal dihapus!');
			$this->datart();
		}
	}

	/*
	 * CRUD Kelola Data Unit Usaha by admin
	 * table: agen
	*/
	public function unitusaha()
	{
		$qry = $this->m_admin->show_table('agen');
		if ($qry->num_rows() > 0) {
			$data['row'] = $qry->result();
		} else {
			$data['row'] = FALSE;
		}
		$data['title'] = 'Unit Usaha';
		$this->load->view('admin/tampil_data_unitusaha', $data);
	}

	public function inputagen()
	{
		$data['title'] = 'Administrator';

		$this->load->view('admin/form_input_agen', $data);
	}

	public function storeunitusaha()
	{
		$formRule = array(
			array(
				'field' => 'nama_agen',
				'label' => '<b>Nama Unit Usaha</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'jenis_usaha',
				'label' => '<b>Jenis Usaha</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'alamat',
				'label' => '<b>Alamat</b>',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($formRule);

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('error', 'Silahkan cek kembali!');
			$this->inputagen();
		} else {
			// lolos validasi
			$inputdataagen = array(
				'nama_agen'   => ucwords($this->input->post('nama_agen')),
				'jenis_usaha' => ucwords($this->input->post('jenis_usaha')),
				'alamat'      => ucwords($this->input->post('alamat'))
			);
			$insert = $this->m_admin->insert_table('agen', $inputdataagen);
			if ($insert) {
				$this->session->set_flashdata('success', 'Data berhasil disimpan!');
				redirect(site_url('admin/unitusaha'));
			} else {
				$this->session->set_flashdata('error', 'Data Gagal disimpan!');
				$this->inputagen();
			}
		}
	}

	public function editunitusaha($id_agen)
	{
		$data['title'] = 'Administrator';
		$where = array('id_agen' => $id_agen);
		$query = $this->m_admin->get_where('agen', $where);
		if ($query->num_rows() == 1) {
			$data['data'] = $query->result();
		} else {
			$data['data'] = NULL;
		}
		$this->load->view('admin/form_edit_agen', $data);
	}

	public function updateunitusaha($id)
	{
		$this->form_validation->set_rules('id', 'ID Agen', 'trim|required|max_length[2]|numeric');
		$id_agen = $this->input->post('id');

		$formRule = array(
			array(
				'field' => 'nama_agen',
				'label' => '<b>Nama Unit Usaha</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'jenis_usaha',
				'label' => '<b>Jenis Usaha</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'alamat',
				'label' => '<b>Alamat</b>',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($formRule);

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('error', 'Silahkan cek kembali!');
			$this->editunitusaha($id_agen);
		} else {
			// lolos validasi
			$dataagen = array(
				'nama_agen'   => $this->input->post('nama_agen'),
				'jenis_usaha' => $this->input->post('jenis_usaha'),
				'alamat'      => $this->input->post('alamat')
			);

			$where = array('id_agen' => $id_agen);
			$update = $this->m_admin->update('agen', $dataagen, $where);

			if ($update) {
				$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
				redirect(site_url('admin/unitusaha'));
			} else {
				$this->session->set_flashdata('error', 'Data Gagal diperbarui!');
				$this->editunitusaha($id_agen);
			}
		}
	}

	public function hapusunitusaha($id_agen)
	{
		$where = array('id_agen'=>$id_agen);
		$delete = $this->m_admin->delete('agen', $where);

		if ($delete) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus!');
			redirect(site_url('admin/unitusaha'));
		} else {
			$this->session->set_flashdata('error', 'Data Gagal dihapus!');
			$this->unitusaha();
		}
	}


	/*
	* Data Lurah
	* Hanya Read
	*/
	public function datalurah()
	{
		$qry = $this->m_admin->get_where('pegawai_kelurahan', array('jabatan' => 'lurah'));
		if ($qry->num_rows() > 0) {
			$data['row'] = $qry;
		} else {
			$data['row'] = FALSE;
		}
		$data['title'] = 'Administrator';
		$this->load->view('admin/tampil_data_lurah', $data);
	}

	/*
	* Data Lurah
	* Hanya Read
	*/
	public function datastaff()
	{
		$qry = $this->m_admin->get_where('pegawai_kelurahan', array('jabatan' => 'staff'));
		if ($qry->num_rows() > 0) {
			$data['row'] = $qry;
		} else {
			$data['row'] = FALSE;
		}
		$data['title'] = 'Administrator';
		$this->load->view('admin/tampil_data_staff', $data);
	}

	/*
	* Jadwal Transfer
	*/
	public function jadwal_transfer()
	{
		$data['title'] = 'Jadwal Transfer';
		$data['row']   = $this->m_admin->show_table('transfer', 'nomor_rt, nomor_rw, tanggal_transfer')->result();

		$this->load->view('admin/tampil_data_jadwal', $data);
	}

	public function inputjadwal()
	{
		$data['title'] = 'Input Jadwal Transfer';

		$this->load->view('admin/form_input_jadwal', $data);
	}

	public function storejadwal()
	{
		$formRule = array(
			array(
				'field' => 'nomor_rt',
				'label' => '<b>Nomor RT</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'nomor_rw',
				'label' => '<b>Nomor RW</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'tanggal_transfer',
				'label' => '<b>Tanggal</b>',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($formRule);

		if ($this->form_validation->run() === FALSE)
		{
			$this->session->set_flashdata('error', 'Silahkan cek kembali!');
			$this->inputjadwal();
		}
		else
		{
			// lolos validasi
			$inputjadwal = array(
				'nomor_rt'         => ucwords($this->input->post('nomor_rt')),
				'nomor_rw'         => ucwords($this->input->post('nomor_rw')),
				'tanggal_transfer' => ucwords($this->input->post('tanggal_transfer'))
			);
			$insert = $this->m_admin->insert_table('transfer', $inputjadwal);

			if ($insert)
			{
				$this->session->set_flashdata('success', 'Data berhasil disimpan!');
				redirect(site_url('admin/jadwal_transfer'));
			}
			else
			{
				$this->session->set_flashdata('error', 'Data Gagal disimpan!');
				$this->inputjadwal();
			}
		}
	}

	public function editjadwal($id)
	{
		$data['title'] = 'Administrator';
		$where = array('id_jadwal' => $id);
		$query = $this->m_admin->get_where('transfer', $where);

		if ($query->num_rows() == 1)
		{
			$data['data'] = $query->result();
		}
		else
		{
			$data['data'] = NULL;
		}
		$this->load->view('admin/form_edit_jadwal', $data);
	}

	public function updatejadwal($id)
	{
		$this->form_validation->set_rules('id', 'ID Transfer', 'trim|required|max_length[2]|numeric');
		$id_jadwal = $this->input->post('id');

		$formRule = array(
			array(
				'field' => 'nomor_rt',
				'label' => '<b>Nomor RT</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'nomor_rw',
				'label' => '<b>Nomor RW</b>',
				'rules' => 'required'
			),
			array(
				'field' => 'tanggal_transfer',
				'label' => '<b>Tanggal Transfer</b>',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($formRule);

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('error', 'Silahkan cek kembali!');
			$this->editjadwal($id_jadwal);
		} else {
			// lolos validasi
			$jadwalTrf = array(
				'nomor_rt'         => $this->input->post('nomor_rt'),
				'nomor_rw'         => $this->input->post('nomor_rw'),
				'tanggal_transfer' => $this->input->post('tanggal_transfer')
			);

			$where = array('id_jadwal' => $id_jadwal);
			$update = $this->m_admin->update('transfer', $jadwalTrf, $where);

			if ($update) {
				$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
				redirect(site_url('admin/jadwal_transfer'));
			} else {
				$this->session->set_flashdata('error', 'Data Gagal diperbarui!');
				$this->editjadwal($id_jadwal);
			}
		}
	}

	public function hapusjadwal($id)
	{
		$where = array('id_jadwal'=>$id);
		$delete = $this->m_admin->delete('transfer', $where);

		if ($delete) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus!');
			redirect(site_url('admin/jadwal_transfer'));
		} else {
			$this->session->set_flashdata('error', 'Data Gagal dihapus!');
			$this->jadwal_transfer();
		}
	}

}
