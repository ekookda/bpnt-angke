<?php defined('BASEPATH') OR exit('No direct script access allowed');

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Lurah extends CI_Controller
{
	var $data = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
		$this->data['title'] = ' Lurah';
	}

	public function index()
	{
		$data['total_warga']       = $this->get_count_data('warga');
		$data['total_berhak']      = $this->get_count_where('warga', array('status' => '0'));
		$data['total_belumBerhak'] = $this->get_count_where('warga', array('status' => '1'));
		$data['total_agen']        = $this->get_count_data('agen');
		$data['total_rt']          = $this->get_count_data('rtrw');
		$data['count']             = $this->count_rt()->result_array();
		$data['title'] = 'Lurah';
		
		$this->load->view('lurah/index', $data);
	}

	public function laporandatawarga()
	{
		$table = "warga";
		$data['title'] = 'Lurah';
		$data['data_tbwarga'] = $this->m_admin->show_table($table, 'rt, status, nama_lengkap');

		$this->load->view('lurah/data_warga', $data);
	}

	public function laporandataagen()
	{
		$table = "agen";
		$data['title'] = 'Administrator';
		$data['data_tbagen'] = $this->m_admin->show_table($table);

		$this->load->view('lurah/data_agen', $data);
	}

	public function report_agen()
	{
		$this->load->library('pdfgenerator');

		$table = "agen";
		$data['agen'] = $this->m_admin->show_table($table)->result_array();

		$html = $this->load->view('lurah/report/laporan_agen', $data, TRUE);
		$this->pdfgenerator->generate($html, 'contoh');
	}

	public function report()
	{
		// instantiate and use the dompdf class
		$dompdf = new Dompdf();
		$loadHtml = $this->load->view('lurah/report/laporan_agen');
		$dompdf->loadHtml($loadHtml);
		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'Potrait');
		// Render the HTML as PDF
		$dompdf->render();
		// Output the generated PDF to Browser
		$dompdf->stream('report', array('Attachment' => 0));
	}

	// method menghitung jumlah data suatu table di database
	public function get_count_data($table)
	{
		$query = $this->m_admin->show_table($table);
		$total = $query->num_rows();
		return $total;
	}

	// method menghitung jumlah data dengan kondisi suatu table di database
	public function get_count_where($table, $where)
	{
		$query = $this->m_admin->get_where($table, $where);
		$total = $query->num_rows();
		return $total;
	}

	public function get_true($table)
	{
		$this->db->where(array('status'=>'1'));
		$query = $this->db->get_where($table);
		return $query;
	}

	public function count_rt()
	{
		$get = $this->m_admin->count_rt();
		return $get;
	}

}
