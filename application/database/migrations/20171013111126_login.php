<?php

class Migration_login extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id_login' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => '30'
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'level' => array(
                'type' => 'ENUM',
                'constraint' => '30'
            )
        ));
        $this->dbforge->add_key('id_login', TRUE);
        $this->dbforge->create_table('logins');
    }

    public function down() {
        $this->dbforge->drop_table('login');
    }

}
