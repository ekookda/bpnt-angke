-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 28 Okt 2017 pada 16.44
-- Versi Server: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bpnt`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `agen`
--

DROP TABLE IF EXISTS `agen`;
CREATE TABLE IF NOT EXISTS `agen` (
  `id_agen` int(11) NOT NULL AUTO_INCREMENT,
  `nama_agen` varchar(30) NOT NULL,
  `jenis_usaha` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id_agen`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `agen`
--

INSERT INTO `agen` (`id_agen`, `nama_agen`, `jenis_usaha`, `alamat`) VALUES
(3, 'Toko Beras Makmur', 'Agen Beras', 'Jl Jembatan  No. 60 Jakarta Pusat'),
(4, 'Toko Jaya Setia', 'Toko Bangunan', 'Jl Jembatan  No. 160 Jakarta Barat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` enum('staff','lurah','rtrw') NOT NULL COMMENT '1=''staff'', 2=''lurah'', 3=''rt/rw''',
  PRIMARY KEY (`username`) USING BTREE,
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`username`, `password`, `level`) VALUES
('admin', '$2y$10$hAbsSfs5DaAyW2T6fAJdvORN1/xRV/jd4LpsVJfuSmXitiCqyaa.i', 'staff'),
('dalima.riyanti', '$2y$10$vhHq4aCqLdLQ6hEv89qKV.H3oxsKdjJWhdh6UdrQ1n0CEWu/USpQi', 'rtrw'),
('elisa97', '$2y$10$cOrchBtz./FMLFDUGjhIROtms7yPFrPxG/vUzoM23KMGDwc1g1FSq', 'rtrw'),
('enashiruddin', '$2y$10$kA8kiHRQpUNZVgX.5kJzjOqzvyoFx7B10YTup6koJfDBdBGYPqyMu', 'rtrw'),
('hasta.hidayat', '$2y$10$R1e7hA6Tjgw4rdNCqnNaOuzsbIW1qdBuaEt536LGjxTyzBG1.wGMq', 'rtrw'),
('jasmin.megantara', '$2y$10$dzOqVVGio2HhW3kaTfQFiuWxcnQFEukgBeaOqfWCRe.MecdkMRsGK', 'rtrw'),
('lurah', '$2y$10$hAbsSfs5DaAyW2T6fAJdvORN1/xRV/jd4LpsVJfuSmXitiCqyaa.i', 'lurah'),
('maryadi.yani', '$2y$10$xoUPnLBJjaFeaqmiQLn5rOwlXHyNsGr78uLjYqhIvPbcmUE1xgmZ.', 'rtrw'),
('qmangunsong', '$2y$10$V.KzuiY2ADVQm/GGmncnteVEsTI0.ljen.93NnN0nXdOmHPaVC64O', 'rtrw'),
('sadina43', '$2y$10$/nmIdfbWZ.v1clbSg1myvOWnNXNVvSYtRWOmOHLe/gRCf.z7ZS016', 'rtrw'),
('syaiful.syuhada', '$2y$10$hAbsSfs5DaAyW2T6fAJdvORN1/xRV/jd4LpsVJfuSmXitiCqyaa.i', 'rtrw'),
('wwahyudin', '$2y$10$RyrdEbgLIQx5VCdzawhOyu61dsirgEET/8jQVkwE/rVeogoxSSrrm', 'rtrw');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai_kelurahan`
--

DROP TABLE IF EXISTS `pegawai_kelurahan`;
CREATE TABLE IF NOT EXISTS `pegawai_kelurahan` (
  `nip` char(18) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('pria','perempuan') NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `nomor_telepon` varchar(13) NOT NULL,
  `jabatan` enum('staff','lurah') NOT NULL,
  PRIMARY KEY (`nip`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai_kelurahan`
--

INSERT INTO `pegawai_kelurahan` (`nip`, `nama_lengkap`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `username`, `email`, `nomor_telepon`, `jabatan`) VALUES
('30812316', 'administrator', 'jakarta', '1987-10-24', 'pria', 'admin', 'admin@gmail.com', '081517406275', 'staff'),
('30812317', 'Misjaya', 'Pandeglang', '1977-09-10', 'pria', 'lurah', 'lurah@gmail.com', '081236991234', 'lurah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `report_penerima`
--

DROP TABLE IF EXISTS `report_penerima`;
CREATE TABLE IF NOT EXISTS `report_penerima` (
  `id_report` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_rt` varchar(3) NOT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id_report`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `report_penerima`
--

INSERT INTO `report_penerima` (`id_report`, `nomor_rt`, `total`) VALUES
(1, '001', NULL),
(2, '002', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rtrw`
--

DROP TABLE IF EXISTS `rtrw`;
CREATE TABLE IF NOT EXISTS `rtrw` (
  `id_rtrw` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(30) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('pria','perempuan') NOT NULL,
  `username` varchar(20) NOT NULL,
  `nomor_telepon` varchar(13) NOT NULL,
  `email` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `rt` varchar(3) NOT NULL,
  PRIMARY KEY (`id_rtrw`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rtrw`
--

INSERT INTO `rtrw` (`id_rtrw`, `nama_lengkap`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `username`, `nomor_telepon`, `email`, `alamat`, `rt`) VALUES
(1, 'Syaiful Syuhada', 'Pandeglang', '1977-10-02', 'pria', 'syaiful.syuhada', '081517406277', 'syaiful.syuhada@gmail.com', 'Tomang Banjir Kanal', '010'),
(11, 'Upik Dongoran', 'Bekasi', '1960-08-30', 'pria', 'elisa97', '6263616491545', 'uhassanah@gmail.co.id', 'Dk. Bakhita No. 732, Jayapura 23172, Lampung', '001'),
(12, 'Kezia Pratiwi', 'Bandar Lampung', '1994-11-05', 'perempuan', 'qmangunsong', '624777740031', 'gantar33@yahoo.co.id', 'Jr. Muwardi No. 911, Bontang 76675, Aceh', '002'),
(13, 'Fitria Mayasari', 'Sabang', '1965-01-19', 'perempuan', 'hasta.hidayat', '6255203792688', 'sitompul.darmaji@yahoo.co.id', 'Ki. Otto No. 378, Padang 20040, JaTim', '003'),
(14, 'Karen Nuraini', 'Cimahi', '1962-09-03', 'pria', 'sadina43', '627119058325', 'yance43@yahoo.com', 'Ki. Abdul No. 382, Sorong 84228, BaBel', '004'),
(15, 'Kasiyah Kani Maryati M.M.', 'Palopo', '1984-09-21', 'perempuan', 'wwahyudin', '6265494575552', 'edi41@yahoo.com', 'Kpg. Veteran No. 579, Bukittinggi 47172, NTT', '005'),
(16, 'Wakiman Jaka Waskita S.Farm', 'Cilegon', '1928-10-10', 'perempuan', 'maryadi.yani', '042880240401', 'melani.unjani@gmail.com', 'Psr. Suryo Pranoto No. 341, Cirebon 62622, NTB', '006'),
(17, 'Dimaz Halim', 'Lhokseumawe', '2016-08-26', 'perempuan', 'jasmin.megantara', '06664122563', 'amardhiyah@yahoo.co.id', 'Kpg. Krakatau No. 735, Kotamobagu 34221, JaBar', '007'),
(18, 'Najib Pranowo', 'Bitung', '1956-12-18', 'perempuan', 'dalima.riyanti', '03293470452', 'ajiman.napitupulu@yahoo.com', 'Psr. K.H. Maskur No. 139, Solok 86857, JaTeng', '008'),
(19, 'Hesti Kusmawati', 'Tanjungbalai', '1935-09-22', 'pria', 'enashiruddin', '06390314927', 'reza.permadi@yahoo.co.id', 'Dk. Supomo No. 838, Cirebon 39914, Gorontalo', '009');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transfer`
--

DROP TABLE IF EXISTS `transfer`;
CREATE TABLE IF NOT EXISTS `transfer` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_rt` varchar(3) NOT NULL,
  `nomor_ktp` char(16) NOT NULL,
  `tanggal_transfer` datetime NOT NULL,
  PRIMARY KEY (`id_jadwal`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transfer`
--

INSERT INTO `transfer` (`id_jadwal`, `nomor_rt`, `nomor_ktp`, `tanggal_transfer`) VALUES
(3, '001', '', '2017-10-26 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `warga`
--

DROP TABLE IF EXISTS `warga`;
CREATE TABLE IF NOT EXISTS `warga` (
  `id_ktp` char(16) NOT NULL COMMENT 'nomor ktp',
  `nama_lengkap` varchar(30) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `rt` varchar(3) NOT NULL,
  `nomor_telepon` varchar(13) NOT NULL,
  `jenis_kelamin` enum('pria','perempuan') NOT NULL,
  `agama` enum('islam','katolik','protestan','hindu','budha') NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0="tidak berhak", 1="berhak"',
  PRIMARY KEY (`id_ktp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `warga`
--

INSERT INTO `warga` (`id_ktp`, `nama_lengkap`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `rt`, `nomor_telepon`, `jenis_kelamin`, `agama`, `status`) VALUES
('2221026844961106', 'Endah Permata', 'Bogor', '1920-09-11', 'Psr. Bambon No. 403, Yogyakarta 49684, Aceh', '004', '6238414185993', 'perempuan', 'islam', '1'),
('2221427952027963', 'Zahra Suryatmi M.Kom.', 'Tidore Kepulauan', '1925-07-16', 'Psr. Baranang Siang No. 581, Bogor 92698, MalUt', '008', '062738469956', 'perempuan', 'budha', '1'),
('2221656519146375', 'Sabrina Salwa Rahimah', 'Jambi', '1930-10-21', 'Gg. Jakarta No. 504, Jayapura 92122, Bali', '008', '021920415741', 'pria', 'katolik', '0'),
('2221871038133461', 'Sarah Kuswandari', 'Administrasi Jakarta', '2003-08-22', 'Gg. M.T. Haryono No. 852, Jambi 46176, JaTim', '002', '6291973580609', 'pria', 'protestan', '1'),
('2318351282396035', 'Galuh Megantara', 'Tanjungbalai', '1943-12-29', 'Gg. Padang No. 17, Solok 64946, SumBar', '004', '065581614572', 'perempuan', 'budha', '1'),
('2321723839523789', 'Lalita Wulan Nasyidah', 'Gunungsitoli', '1948-07-02', 'Jln. K.H. Wahid Hasyim (Kopo) No. 262, Kediri 36313, JaBar', '005', '623638019008', 'perempuan', 'hindu', '1'),
('2339217161889835', 'Vera Susanti', 'Denpasar', '1965-03-15', 'Dk. Basoka No. 141, Pekalongan 34177, Jambi', '009', '05360348674', 'pria', 'hindu', '0'),
('2421131073612186', 'Reza Raditya Pradana', 'Metro', '1980-12-05', 'Dk. Gajah Mada No. 120, Cilegon 68669, JaBar', '007', '6282891299064', 'pria', 'islam', '1'),
('2433461683221664', 'Kartika Pia Padmasari S.I.Kom', 'Bontang', '1944-12-03', 'Psr. Banal No. 872, Manado 44279, JaBar', '004', '625837670550', 'pria', 'islam', '0'),
('2459061296205853', 'Harja Prayoga Ardianto M.Ak', 'Surakarta', '1922-03-27', 'Jr. Imam Bonjol No. 273, Tidore Kepulauan 48491, NTB', '004', '0893963932', 'pria', 'islam', '0'),
('2461772095150134', 'Makuta Sihombing S.Kom', 'Mojokerto', '1954-06-25', 'Ds. Jamika No. 264, Padang 83330, KalTim', '005', '042793238851', 'pria', 'budha', '0'),
('2466347225391026', 'Patricia Ida Lailasari', 'Pekalongan', '1940-06-23', 'Ki. Bak Air No. 538, Prabumulih 20575, Lampung', '002', '0896206387', 'perempuan', 'budha', '0'),
('2535641932899033', 'Sabrina Winarsih', 'Langsa', '1933-09-03', 'Jr. Ahmad Dahlan No. 763, Solok 91989, JaBar', '003', '6236603514054', 'pria', 'budha', '0'),
('2543020025762418', 'Lembah Sirait', 'Pekanbaru', '1980-10-28', 'Ds. Kyai Gede No. 956, Bandar Lampung 66915, SumBar', '009', '05600668862', 'pria', 'hindu', '0'),
('2578336117612131', 'Nilam Puti Hartati', 'Ambon', '1945-06-20', 'Gg. Madrasah No. 97, Makassar 22617, SumSel', '002', '0269395970', 'perempuan', 'protestan', '0'),
('2583827941837152', 'Siska Carla Laksita', 'Tanjungbalai', '1985-03-21', 'Kpg. Arifin No. 581, Bau-Bau 78894, KepR', '004', '030397912374', 'pria', 'protestan', '0'),
('2592937907226826', 'Rahmat Hutapea S.Pt', 'Samarinda', '1964-09-07', 'Ds. Barat No. 835, Jayapura 74402, JaTeng', '001', '6272024999264', 'perempuan', 'budha', '0'),
('2597692267041605', 'Teguh Eman Tampubolon', 'Dumai', '1928-08-22', 'Jln. Baya Kali Bungur No. 604, Langsa 57957, PapBar', '002', '05451987635', 'pria', 'hindu', '0'),
('2615342002520699', 'Amelia Diana Nuraini', 'Manado', '1928-05-17', 'Jln. Gotong Royong No. 659, Mojokerto 90213, SumUt', '001', '62879376588', 'perempuan', 'hindu', '1'),
('2720026408567980', 'Ratih Jasmin Astuti', 'Palangka Raya', '1930-06-25', 'Ds. Moch. Toha No. 14, Administrasi Jakarta Timur 63431, JaTeng', '005', '090439816697', 'perempuan', 'hindu', '0'),
('2720185001350978', 'Ega Marbun', 'Tarakan', '2010-06-05', 'Gg. Bhayangkara No. 428, Serang 80194, Maluku', '005', '09712108746', 'perempuan', 'protestan', '0'),
('2720414281751626', 'Zahra Wirda Zulaika S.Kom', 'Parepare', '1997-04-15', 'Ds. Flores No. 858, Jambi 18427, Lampung', '008', '622028756854', 'perempuan', 'protestan', '1'),
('2720428083136430', 'Yuliana Silvia Yolanda S.T.', 'Administrasi Jakarta', '1927-07-22', 'Jr. Raya Setiabudhi No. 60, Tebing Tinggi 31672, SumBar', '006', '6299641451329', 'pria', 'islam', '0'),
('2720532195333700', 'Yono Samsul Pratama', 'Banda Aceh', '1938-05-03', 'Jr. Eka No. 704, Jambi 46294, KalTim', '002', '04421445492', 'perempuan', 'budha', '0'),
('2720556771445521', 'Karen Melani', 'Administrasi Jakarta', '1926-04-19', 'Dk. Jayawijaya No. 113, Bengkulu 40460, DKI', '004', '049498165473', 'perempuan', 'islam', '0'),
('2720703207853874', 'Sidiq Timbul Natsir', 'Batam', '1948-08-04', 'Ds. Bagas Pati No. 206, Pangkal Pinang 42431, BaBel', '008', '6259448616249', 'perempuan', 'islam', '0'),
('2720832937698075', 'Betania Ayu Lailasari', 'Depok', '1956-10-23', 'Ki. Lembong No. 263, Cilegon 33039, Lampung', '008', '04014143857', 'perempuan', 'budha', '0'),
('2720845059334535', 'Marwata Hardi Tarihoran', 'Sukabumi', '1927-03-11', 'Gg. Diponegoro No. 884, Blitar 50133, JaBar', '008', '061904268798', 'pria', 'islam', '1'),
('2720877836309761', 'Keisha Hastuti', 'Sorong', '1969-03-17', 'Ds. Siliwangi No. 526, Binjai 15131, KalTim', '004', '6288942346222', 'perempuan', 'budha', '1'),
('343198443853237', 'Gandi Simon Mandala S.Pt', 'Bandar Lampung', '1921-09-07', 'Psr. Rajawali Barat No. 998, Bontang 12785, KalTim', '003', '02232270867', 'perempuan', 'islam', '1'),
('346126720309372', 'Almira Kani Susanti', 'Pontianak', '1949-08-23', 'Kpg. Jagakarsa No. 67, Salatiga 21346, SulSel', '004', '6243574787189', 'perempuan', 'hindu', '0'),
('347178116650561', 'Margana Kuswoyo', 'Bau-Bau', '1942-01-02', 'Dk. Monginsidi No. 642, Palopo 66064, SumUt', '001', '627273107456', 'pria', 'hindu', '0'),
('348830524826766', 'Tirta Pratama', 'Tangerang Selatan', '2012-10-09', 'Psr. Sukabumi No. 498, Binjai 91662, Bengkulu', '004', '623141192198', 'pria', 'islam', '0'),
('348855302289432', 'Kayun Cakrawala Jailani S.E.I', 'Blitar', '2005-01-27', 'Ki. Basoka Raya No. 325, Kupang 33741, BaBel', '003', '62821519852', 'perempuan', 'budha', '0'),
('370176767044993', 'Oman Wahyudin', 'Prabumulih', '1939-06-10', 'Ds. Rajawali No. 903, Payakumbuh 13219, DKI', '004', '072356879983', 'pria', 'hindu', '0'),
('376213355171054', 'Raharja Kamidin Setiawan S.H.', 'Lhokseumawe', '1931-05-20', 'Kpg. Baing No. 702, Pariaman 14051, KalTeng', '009', '09696461406', 'perempuan', 'hindu', '1'),
('376569868137399', 'Hafshah Prastuti', 'Yogyakarta', '1925-07-23', 'Dk. Rumah Sakit No. 400, Cimahi 12955, KepR', '005', '09002958999', 'pria', 'islam', '1'),
('378841289776370', 'Ian Eman Halim', 'Semarang', '2013-11-15', 'Dk. Umalas No. 897, Bandung 14517, SulBar', '002', '08606718551', 'pria', 'islam', '0'),
('4024007101080017', 'Padma Novitasari S.Psi', 'Tual', '1986-02-09', 'Gg. Banceng Pondok No. 393, Tangerang 18156, SulTeng', '006', '06292837965', 'perempuan', 'katolik', '1'),
('4024007113547', 'Vega Utama', 'Samarinda', '1962-08-07', 'Ki. Basket No. 482, Malang 92466, JaTeng', '004', '083616332993', 'pria', 'islam', '0'),
('4024007143432267', 'Labuh Najmudin', 'Pagar Alam', '1983-08-20', 'Jr. Ters. Buah Batu No. 994, Tegal 91610, SulUt', '007', '06387650653', 'pria', 'protestan', '1'),
('4024007144896882', 'Jagaraga Sihombing', 'Ambon', '1935-04-27', 'Kpg. Arifin No. 651, Samarinda 33259, Banten', '005', '083995098986', 'perempuan', 'budha', '0'),
('4024007183901833', 'Jarwi Jagaraga Ardianto', 'Blitar', '2001-12-15', 'Psr. Kusmanto No. 383, Yogyakarta 69186, KalTim', '008', '6223408893008', 'perempuan', 'hindu', '0'),
('4024007184178225', 'Unggul Suwarno', 'Solok', '1973-01-27', 'Psr. Jaksa No. 44, Palembang 70527, Aceh', '008', '626555784889', 'pria', 'protestan', '1'),
('4024007185164', 'Yessi Malika Utami', 'Bima', '1946-10-13', 'Psr. Bakin No. 603, Sabang 43788, SulBar', '009', '62856327152', 'pria', 'katolik', '0'),
('4174113401671816', 'Ade Aryani', 'Surabaya', '2008-10-17', 'Ki. Suniaraja No. 326, Kediri 73760, SulTeng', '010', '03550638002', 'pria', 'budha', '1'),
('4178006650683', 'Jais Tamba', 'Dumai', '1976-02-09', 'Gg. Pattimura No. 949, Manado 66649, SumUt', '001', '624852229683', 'perempuan', 'hindu', '1'),
('4485298242784', 'Amelia Usamah M.TI.', 'Semarang', '1943-09-28', 'Jr. HOS. Cjokroaminoto (Pasirkaliki) No. 187, Administrasi Jakarta Barat 96566, Maluku', '003', '6235933273522', 'pria', 'hindu', '0'),
('4485390361821', 'Cici Susanti', 'Palopo', '1977-05-29', 'Ds. Bass No. 6, Bogor 14717, SumUt', '009', '03930520163', 'pria', 'katolik', '1'),
('4485621408789', 'Mala Aurora Nasyiah', 'Pariaman', '1917-11-05', 'Jln. Ketandan No. 143, Tebing Tinggi 92009, Aceh', '008', '628626751148', 'pria', 'budha', '0'),
('4532206417626', 'Citra Andriani', 'Tanjung Pinang', '1946-09-01', 'Jln. Gedebage Selatan No. 261, Dumai 26860, JaTim', '010', '6230381899420', 'perempuan', 'protestan', '0'),
('4532401775369043', 'Jati Winarno', 'Administrasi Jakarta', '1941-10-12', 'Ds. Rajawali Barat No. 513, Kupang 16132, JaBar', '006', '090118928214', 'perempuan', 'hindu', '1'),
('4539202103944', 'Chandra Sirait', 'Sungai Penuh', '1943-02-24', 'Gg. Wahid No. 414, Batu 19183, SumSel', '002', '041882683997', 'pria', 'budha', '1'),
('4539923368840299', 'Atmaja Wahyudin', 'Batam', '1980-11-10', 'Kpg. Hang No. 642, Medan 95335, Riau', '007', '625609053649', 'pria', 'budha', '0'),
('4539951931941', 'Uchita Cornelia Safitri', 'Payakumbuh', '1983-07-07', 'Kpg. Bahagia No. 507, Bau-Bau 18129, Banten', '006', '03632290577', 'pria', 'islam', '1'),
('4556056332753260', 'Vivi Kamaria Melani', 'Bitung', '1945-09-04', 'Psr. Dr. Junjunan No. 308, Subulussalam 14274, DKI', '004', '6296565411874', 'pria', 'islam', '0'),
('4556147684349953', 'Galur Lazuardi', 'Payakumbuh', '1991-03-29', 'Ki. Jakarta No. 31, Gorontalo 49302, BaBel', '006', '626687606107', 'pria', 'islam', '0'),
('4556777480572', 'Jaya Mangunsong', 'Batu', '1929-10-08', 'Psr. Labu No. 248, Pangkal Pinang 57924, JaTeng', '002', '6232933688658', 'pria', 'katolik', '0'),
('4716249566170007', 'Zelaya Dian Nasyiah S.Psi', 'Administrasi Jakarta', '2016-02-02', 'Dk. Dewi Sartika No. 949, Pagar Alam 33850, NTT', '010', '6257298519788', 'pria', 'budha', '1'),
('4716451767740', 'Garda Gada Waskita S.Sos', 'Pagar Alam', '1979-03-17', 'Psr. Salam No. 834, Manado 75560, KalSel', '004', '6234708319317', 'perempuan', 'hindu', '0'),
('4716552990936', 'Fitria Wastuti M.M.', 'Tasikmalaya', '1930-06-15', 'Kpg. Laksamana No. 965, Mataram 49487, KalBar', '008', '6231682327285', 'perempuan', 'katolik', '1'),
('4716665657174', 'Mulyono Mariadi Ardianto', 'Padang', '1931-03-28', 'Ki. Gajah Mada No. 754, Kotamobagu 76244, JaTeng', '010', '627845537769', 'pria', 'katolik', '0'),
('4716778671419', 'Kajen Marpaung', 'Kupang', '1990-06-15', 'Gg. Ters. Jakarta No. 755, Gunungsitoli 38440, NTT', '003', '624165618060', 'perempuan', 'protestan', '1'),
('4716885090123', 'Slamet Zulkarnain', 'Magelang', '1944-12-03', 'Psr. Madrasah No. 420, Gunungsitoli 35632, JaBar', '004', '6256215182825', 'pria', 'hindu', '1'),
('4716896021117', 'Eko Wasita S.H.', 'Probolinggo', '1980-02-05', 'Gg. Surapati No. 441, Malang 35242, Lampung', '003', '6236908184200', 'perempuan', 'hindu', '1'),
('4716967488758118', 'Vanesa Utami S.I.Kom', 'Samarinda', '1943-12-30', 'Ki. Bakit  No. 570, Tasikmalaya 65742, JaBar', '010', '626279349481', 'perempuan', 'katolik', '1'),
('4916009560423812', 'Juli Puspita', 'Palu', '1997-04-06', 'Ki. Sudirman No. 211, Jambi 10784, Bengkulu', '002', '622007964245', 'perempuan', 'hindu', '0'),
('4916178919571181', 'Uli Hasanah', 'Metro', '1931-01-22', 'Dk. Industri No. 483, Tanjung Pinang 54485, DIY', '002', '629102936623', 'pria', 'hindu', '0'),
('4916475820240', 'Drajat Prakasa', 'Parepare', '1990-08-10', 'Kpg. Katamso No. 679, Pangkal Pinang 62349, SumSel', '009', '08960232075', 'pria', 'hindu', '0'),
('4916874883955', 'Tugiman Rama Nashiruddin M.M.', 'Makassar', '1925-04-16', 'Psr. Sugiono No. 445, Pangkal Pinang 12161, JaTeng', '005', '623379115703', 'pria', 'islam', '1'),
('4929084780655', 'Gilang Asmadi Salahudin S.I.Ko', 'Binjai', '1946-07-13', 'Dk. Ekonomi No. 863, Sabang 76674, DIY', '007', '04286532345', 'perempuan', 'protestan', '1'),
('4929630101680092', 'Gara Nainggolan', 'Metro', '1942-04-15', 'Jr. Orang No. 864, Kendari 41784, KalTeng', '001', '02421602337', 'perempuan', 'islam', '1'),
('4929970377098714', 'Muhammad Caraka Sihombing M.Ak', 'Kupang', '1936-01-20', 'Ds. Imam No. 186, Surabaya 21230, Bengkulu', '003', '62215200574', 'pria', 'islam', '0'),
('4929994907281', 'Jarwa Maulana M.M.', 'Bekasi', '1946-06-30', 'Jln. Raden Saleh No. 322, Balikpapan 65617, JaTeng', '003', '6274448882757', 'perempuan', 'hindu', '1'),
('5104268929540521', 'Rizki Hidayat M.Ak', 'Administrasi Jakarta', '1937-05-12', 'Ki. Abdullah No. 364, Prabumulih 99720, SulSel', '007', '038498177707', 'pria', 'protestan', '0'),
('5110542918150381', 'Nilam Yuliarti S.E.', 'Pekanbaru', '1947-09-12', 'Jr. Otto No. 540, Singkawang 49029, Jambi', '005', '6282013143190', 'pria', 'budha', '0'),
('5120891436153684', 'Galih Thamrin S.Kom', 'Tual', '1964-02-17', 'Ds. Babah No. 219, Bontang 30114, SulBar', '009', '03152349368', 'perempuan', 'budha', '0'),
('5129410532566169', 'Elon Suwarno', 'Ambon', '1927-10-22', 'Dk. Jaksa No. 462, Makassar 38075, JaBar', '006', '05996415206', 'perempuan', 'hindu', '0'),
('5135344684453904', 'Hana Novi Pratiwi M.Kom.', 'Tegal', '1929-07-04', 'Kpg. Badak No. 423, Bima 31175, Riau', '007', '022855943862', 'pria', 'hindu', '1'),
('5158571457647765', 'Daliman Budiyanto M.TI.', 'Tanjung Pinang', '1942-02-28', 'Ki. Sunaryo No. 942, Palembang 49294, SumUt', '008', '6288348581524', 'pria', 'protestan', '1'),
('5191443648723070', 'Setya Pradana', 'Pekalongan', '1963-09-27', 'Jln. Pattimura No. 668, Administrasi Jakarta Utara 15373, Jambi', '008', '097960835631', 'perempuan', 'budha', '0'),
('5202654475634514', 'Okta Cakrawala Widodo M.TI.', 'Banjarmasin', '1970-10-03', 'Ki. Monginsidi No. 866, Cilegon 19555, SumSel', '005', '0274075439', 'perempuan', 'islam', '1'),
('5234748862710515', 'Ivan Olga Situmorang', 'Sorong', '2011-06-18', 'Ki. Dipenogoro No. 115, Sukabumi 68318, Maluku', '007', '07667090214', 'pria', 'islam', '0'),
('5277104570491188', 'Sarah Oktaviani', 'Palu', '2001-10-22', 'Ki. Diponegoro No. 588, Lhokseumawe 57406, SumSel', '006', '08913907597', 'pria', 'hindu', '0'),
('5333974121380532', 'Lega Anggriawan S.E.I', 'Banda Aceh', '1921-04-10', 'Kpg. Baranangsiang No. 16, Kediri 57362, DKI', '004', '626617955923', 'pria', 'budha', '0'),
('5409158922419678', 'Ghani Ega Ardianto', 'Balikpapan', '1938-04-16', 'Psr. Wahidin Sudirohusodo No. 623, Sibolga 19131, KalTim', '009', '625885755032', 'pria', 'budha', '1'),
('5437440314981069', 'Lanjar Samosir S.I.Kom', 'Jayapura', '1935-03-05', 'Ds. Supomo No. 719, Sabang 79645, SulSel', '002', '626462470216', 'perempuan', 'protestan', '1'),
('5464572682958000', 'Chandra Saadat Budiman S.H.', 'Pariaman', '2006-04-25', 'Dk. Arifin No. 329, Batam 25503, SulTeng', '009', '050019764656', 'perempuan', 'budha', '1'),
('5468147319066232', 'Umay Hidayat S.Pd', 'Jambi', '1918-03-26', 'Dk. Otto No. 516, Tanjung Pinang 43549, Papua', '009', '09153036002', 'pria', 'islam', '0'),
('5501238015945461', 'Maimunah Agustina S.Pd', 'Blitar', '1988-01-24', 'Jln. Cihampelas No. 322, Banjarbaru 97347, KepR', '003', '042980377638', 'pria', 'katolik', '1'),
('5542389908323539', 'Ida Hartati', 'Bukittinggi', '1931-09-15', 'Ds. Ters. Kiaracondong No. 184, Batam 25776, SulBar', '007', '051255967873', 'perempuan', 'protestan', '1'),
('5569281981299553', 'Ifa Lestari', 'Bogor', '1956-06-19', 'Jln. Hang No. 886, Pekalongan 24774, KalBar', '007', '622959931051', 'pria', 'islam', '1'),
('6011067944401291', 'Rini Nurdiyanti', 'Prabumulih', '1921-12-03', 'Ds. Dipatiukur No. 21, Tual 68668, SulTra', '008', '626811938091', 'perempuan', 'budha', '1'),
('6011096474787167', 'Betania Indah Wulandari S.Gz', 'Binjai', '1945-03-25', 'Jln. Ters. Pasir Koja No. 363, Banjar 86265, PapBar', '003', '0203678988', 'pria', 'budha', '0'),
('6011192118882751', 'Budi Prasetyo Winarno S.Pt', 'Tangerang Selatan', '1951-04-15', 'Kpg. Bara Tambar No. 198, Administrasi Jakarta Timur 83953, DKI', '005', '052708726048', 'pria', 'katolik', '0'),
('6011353553040194', 'Galih Hutasoit', 'Medan', '1926-06-09', 'Jr. Sunaryo No. 895, Parepare 51822, SulTeng', '009', '03650854634', 'perempuan', 'hindu', '1'),
('6011408864221899', 'Jatmiko Prakasa', 'Batu', '1998-11-06', 'Gg. R.M. Said No. 558, Tomohon 91884, NTB', '010', '0820980771', 'perempuan', 'hindu', '1'),
('6011439442150003', 'Adikara Marbun', 'Bukittinggi', '1997-06-25', 'Ki. Ir. H. Juanda No. 125, Ternate 24087, SulTra', '009', '071490413676', 'perempuan', 'hindu', '1'),
('6011516718995979', 'Almira Andriani', 'Madiun', '1933-01-18', 'Gg. Bak Air No. 711, Singkawang 14579, Bali', '005', '628398846438', 'perempuan', 'budha', '0'),
('6011601821399341', 'Aisyah Siska Widiastuti M.TI.', 'Tangerang', '1950-10-09', 'Dk. Imam Bonjol No. 50, Tanjungbalai 46597, JaBar', '003', '6285062754496', 'perempuan', 'hindu', '0');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pegawai_kelurahan`
--
ALTER TABLE `pegawai_kelurahan`
  ADD CONSTRAINT `username_kelurahan` FOREIGN KEY (`username`) REFERENCES `login` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `rtrw`
--
ALTER TABLE `rtrw`
  ADD CONSTRAINT `username_rtrw` FOREIGN KEY (`username`) REFERENCES `login` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
